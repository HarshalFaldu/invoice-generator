<?php

session_start();
if(!isset($_SESSION['email'])){
  header("location:dashboard.php");
}

$error = "";
require('./connection.php');

require('vendor/autoload.php');

use Rakit\Validation\Validator;
$validator = new Validator;

$validation = $validator->make($_POST + $_FILES, [
  'name'                  => 'required',
  'email'                 => 'required',
  'phone'                 => 'required',
  'gstin'                 => 'required',
  'currency'              => 'required',
  'address'               => 'required',
  'place_to_supply'       => 'required',
  'state_code'            => 'required',
]);
$validation->validate();
if($_POST){
  if ($validation->fails()) {
    $errors = $validation->errors();
    $errors_array = $errors->firstOfAll();
    $_SESSION['name_error'] = $errors_array['name'];
    $_SESSION['email_error'] = $errors_array['email'];
    $_SESSION['phone_error'] = $errors_array['phone'];
    $_SESSION['gstin_error'] = $errors_array['gstin'];
    $_SESSION['currency_error'] = $errors_array['currency'];
    $_SESSION['address_error'] = $errors_array['address'];
    $_SESSION['place_error'] = $errors_array['place_to_supply'];
    $_SESSION['state_error'] = $errors_array['state_code'];

    if($errors_array['name']==="The Name is required" || $errors_array['address']==="The Address is required" || $errors_array['place_to_supply']==="The Place to supply is required" || $errors_array['state_code']==="The State code is required" ){
      $_SESSION['error_in_adding'] = "Fields can't be empty";
    }else{
      $_SESSION['error_in_adding'] = "Error while Inseting data";
    }
    header("location:clients.php");
    exit;
  }else{
    $sql = 'INSERT INTO clients (name,email,phone,gstin,currency,address,place_of_supply,state_code,is_deleted) Values ("'. $_POST['name'] .'","'. $_POST['email'] .'",'. $_POST['phone'] .',"'. $_POST['gstin'] .'","'. $_POST['currency'] .'","'. $_POST['address'].'","'.$_POST['place_to_supply'].'","'.$_POST['state_code'].'","active");';
    
    if (!$conn->query($sql) === TRUE) {
      if(strpos($conn->error,"Duplicate") !== false){
        $error = "Duplicate entry found";
      }else{
        $error = "Error inserting data";
      }
      $_SESSION['error_in_adding'] = $error;
    } else{
      $sql3 = 'SELECT max(id) as c_id from clients;';
      $result = $conn -> query($sql3);
    
      if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) { 
          for($i = 0; $i < count($_POST['tax']); $i++){
              $sql2 = "INSERT INTO client_taxes (client_id,tax_name,percentage) VALUES ('".$row['c_id']."','".$_POST['tax'][$i]."',".$_POST['tax_rate'][$i].");";
              if (!$conn->query($sql2)) {
                $error = "Error inserting da";
            } else{
              $_SESSION['add_success'] = "User added successfully";
            }
          }
        }
      }
    }
  
  }
    $conn->close();
  }

?>


<!DOCTYPE html>
<html>

<?php include('./head_files.php') ?>

<body class="body">
  <!-- Sidenav -->
<?php 
include('./sidebar.php');
include('./header.php');?>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    
    <!-- Header -->
    <!-- Header -->
    <div class="header  pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-baseline py-5">
            <div class="col-lg-7 col-7">
              <h6 class="h2  d-inline-block mb-0">All Clients</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">All Clients</li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-5 col-5 text-right ">
              <button class="btn btn-neutral addClient" type="button"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp; Add New Client</button>
              <a class="btn btn-danger " type="button" href="./removed_client.php"><i class="fa fa-trash-o"></i>&nbsp;&nbsp; Removed Clients</a>
            </div>
            <!-- <div class="col-lg-3 col-5 text-right" style="max-width:20%;">
            </div> -->
            <?php 
            if(isset($_SESSION['error_in_adding'])){
            ?>
            <span style="margin-left:14px;width:400px;color:white;" class="alert alert-danger fa fa-times"><?php if(isset($_SESSION['error_in_adding'])){ echo "    ".$_SESSION['error_in_adding']; }?></span>
            <?php
            }
             
            if(isset($_SESSION['add_success'])){
            ?>
            <span style="margin-left:14px;width:400px;color:white;" class="alert alert-success fa fa-check"><?php if(isset($_SESSION['add_success'])){ echo "    ".$_SESSION['add_success']; }?></span>
            <?php
            }
            ?>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6 table-responsive">
      
    <table id="my-example" class="table align-items-center table-flush">
    <thead>
      <tr>
          <th>Id</th>
          <th>Name</th>
          <th>Email</th>
          <th>Phone</th>
          <th>GSTIN</th>
          <th>Currency</th>
          <th>Address</th>
          <th>Place To Supply</th>
          <th>State Code</th>
          <th>Tax included</th>
          <th>Edit</th>
          <th>Delete</th>
      </tr>
    </thead>
  </table>
    </div>
  </div>

   <!-- Add Clients code -->

  <div class="popup_registration inactive">
            <div class="main-content_2">
    
            <div class="container-fluid">
            
            <div class="container pt-7">
            <div class="row justify-content-center">
                <div class="col-lg-7 col-md-7">
                <div class="card bg-secondary border-0 mb-0">
                    <div class="card-body px-lg-5 py-lg-5">
                    
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST" name="client_form">
                        <div class="form-group mb-3">
                        <div class="input-group input-group-merge ">
                            <!-- <div class="input-group-prepend"> -->
                            <label for="name" class="input-group-text" style="width:130px;color:black;">Name: </label>
                            <input class="form-control"  type="text" name="name" id="name">
                            <!-- </div> -->
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['name_error'])){echo$_SESSION['name_error'];} ?></span>
                        </div>

                        <div class="form-group mb-3">
                        <div class="input-group input-group-merge">
                            <!-- <div class="input-group-prepend"> -->
                            <label for="email" class="input-group-text" style="width:130px;color:black;">Email: </label>
                            <!-- </div> -->
                            <input class="form-control" type="email" name="email" id="email">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['email_error'])){echo$_SESSION['email_error'];} ?></span>
                        </div>
                        
                        <div class="form-group mb-3">
                        <div class="input-group input-group-merge">
                            <!-- <div class="input-group-prepend"> -->
                            <label for="phone"  class="input-group-text" style="width:130px;color:black;">Phone No.: </label>
                            <!-- </div> -->
                            <input class="form-control" type="tel" id="phone" name="phone" pattern="[0-9]{10}" required>
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['phone_error'])){echo$_SESSION['phone_error'];} ?></span>
                        </div>
                        
                        <div class="form-group mb-3">
                        <div class="input-group input-group-merge">
                            <!-- <div class="input-group-prepend"> -->
                            <label for="gstin"  class="input-group-text" style="width:130px;color:black;">GSTIN: </label>
                            <!-- </div> -->
                            <input class="form-control" type="text" id="gstin" name="gstin" required>
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['gstin_error'])){echo$_SESSION['gstin_error'];} ?></span>
                        </div>

                        <div class="form-group">
                        <div class="input-group input-group-merge">
                        <label for="address"  class="input-group-text" style="width:130px;color:black;">Address: </label>
                            <input class="form-control"  type="text" name="address" id="address">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['address_error'])){echo$_SESSION['address_error'];} ?></span>
                        </div>
                        
                        <div class="form-group">
                        <div class="input-group input-group-merge">
                        <label for="currency"  class="input-group-text" style="width:130px;color:black;">Currency: </label>
                        
                              <select name="currency" class="pd-currencies form-control"></select>
                            
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['currency_error'])){echo$_SESSION['currency_error'];} ?></span>
                        </div>

                        <div class="form-group">
                        <div class="input-group input-group-merge">
                            <label for="place_to_supply"  class="input-group-text" style="width:130px;color:black;">Place to supply: </label>
                            <input class="form-control" type="text" name="place_to_supply" id="place_to_supply">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['place_error'])){echo$_SESSION['place_error'];} ?></span>
                        </div>
                        
                        <div class="form-group">
                        <div class="input-group input-group-merge">
                        <label for="state_code"  class="input-group-text" style="width:130px;color:black;"> State code: </label>
                            <input class="form-control" type="text" name="state_code" id="state_code">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['state_error'])){echo$_SESSION['state_error'];} ?></span>
                        </div>
                        
                        <!-- Hidden div -->
                        <div class="clone_hidden"> 
                        <div class="form-group edit_tax">
                        <div class="input-group input-group-merge input-group-alternative">
                            <label for="tax"  class="input-group-text" style="width:70px;color:black;">Tax: </label>
                            <input class="form-control" placeholder="Tax_Name" type="text" name="tax[]" >
                            <label for="tax_rate"  class="input-group-text" style="width:90px;color:black;"> Tax rate: </label>
                            <input class="form-control" placeholder="Tax_Percentage" type="number" name="tax_rate[]"  min="0" max="100">       
                            <button type="button" class="btn btn-success my-1 addTax" >Add Tax</button>                     
                        </div>
                        </div>
                        </div>

                        <!-- Taxes -->
                        <div class="clone"> 
                        <div class="form-group edit_tax">
                        <div class="input-group input-group-merge input-group-alternative">
                            <label for="tax"  class="input-group-text" style="width:70px;color:black;">Tax: </label>
                            <input class="form-control" placeholder="Tax_Name" type="text" name="tax[]" >
                            <label for="tax_rate"  class="input-group-text" style="width:90px;color:black;"> Tax rate: </label>
                            <input class="form-control" placeholder="Tax_Percentage" type="number" name="tax_rate[]" min="0" max="100">       
                            <button type="button" class="btn btn-primary my-1 addTax" >Add Tax</button>                     
                        </div>
                        </div>
                        </div>

                        <div class="text-center">
                        <input type="submit" class="btn btn-success my-4" placeholder="Sign in">
                        <a type="button" class="btn btn-danger my-4" href="./clients.php">Cancel</a>
                        </div>
                    </form>
                    </div>
                </div>
                <div class="row mt-3">
            
                </div>
                </div>
            </div>
            </div>
            </div>
        </div>
          </div>
  <?php include('./footer.php'); ?>
  <script>
  var clientIds = [];
  var dt = $('#my-example').DataTable({
    scrollY:        "270px",
    scrollX:        true,
    scrollCollapse: true,
    fixedColumns: true
  });
    $.ajax({
        url: "./get_data.php",
        dataType: "JSON",
        method: "POST",
        data: {operation:"get_client"},
        success : function(jsonObject){
         console.log(jsonObject);
            for(var i = 0;i < jsonObject.data.length; i++){
                var row = [i+1,jsonObject.data[i].name,jsonObject.data[i].email,jsonObject.data[i].phone,jsonObject.data[i].gstin,jsonObject.data[i].currency,jsonObject.data[i].address,jsonObject.data[i].place_of_supply,jsonObject.data[i].state_code,jsonObject.data[i].tax_included]
                clientIds.push(jsonObject.data[i].id)
                // document.cookie = "var1="+row[2];
                var edit_client = `<a class='edit btn btn-info' href="./edit_client.php?email=${jsonObject.encode[i]}"><i class="fa fa-pencil"></i></a>`;
                dt.row.add([row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8],row[9],edit_client,`<a class='deleteclient btn btn-danger'><i class="fa fa-trash"></i></a>`]).draw();
            }
        }
    });
    
    
    new Pidie();
  </script>


 <?php
    unset($_SESSION['error_in_adding']);
    unset($_SESSION['add_success']);
    unset($_SESSION['name_error']);
    unset($_SESSION['phone_error']);
    unset($_SESSION['email_error']);
    unset($_SESSION['gstin_error']);
    unset($_SESSION['currency_error']);
    unset($_SESSION['address_error']);
    unset($_SESSION['place_error']);
    unset($_SESSION['state_error']);
 ?>

</body>

</html>
