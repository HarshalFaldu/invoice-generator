
<?php

session_start();
if(!isset($_SESSION['email'])){
  header("location:dashboard.php");
}
require('./connection.php');
$sql = 'select count(id) as total_client from clients where is_deleted = "active"';
$result = $conn->query($sql);
$row = mysqli_fetch_array($result);

$sql1 = 'select count(id) as total_projects from projects where is_deleted = "active"';
$result1 = $conn->query($sql1);
$row1 = mysqli_fetch_array($result1);

$sql2 = 'select count(id) as total_invoices from invoices where is_deleted = "active"';
$result2 = $conn->query($sql2);
$row2 = mysqli_fetch_array($result2);
require('./connection.php');
?>

<!DOCTYPE html>
<html lang="en">
<?php include('./head_files.php') ?>
<body class="body">
<?php 
include('./sidebar.php');
include('./header.php');?>
<div class="header  pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
              <h6 class="h2  d-inline-block mb-0" style="margin-left: 18px;">Dashboard</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
              
            </div>
          </div>
          <!-- Card stats -->
          <div class="row">
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Active Clients</h5>
                      <span class="h2 font-weight-bold mb-0"><?php echo $row['total_client'] ?></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                        <i class="fa fa-user fa-3x"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                  <a href="http://localhost:8000/invoice_task/clients.php" style="text-decoration: none;"><span class="text-success mr-2"><i class="fa fa-arrow-up"> Click here to visit </i></span></a>
                    <span class="text-nowrap"></span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Active Projects</h5>
                      <span class="h2 font-weight-bold mb-0"><?php echo $row1['total_projects'] ?></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                        <i class="fa fa-tasks fa-3x"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                  <a href="http://localhost:8000/invoice_task/projects.php" style="text-decoration: none;"><span class="text-success mr-2"><i class="fa fa-arrow-up"> Click here to visit</i></span></a>
                    <span class="text-nowrap"></span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Total Invoices</h5>
                      <span class="h2 font-weight-bold mb-0"><?php echo $row2['total_invoices'] ?></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                        <i class="fa fa-money fa-3x"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                  <a href="http://localhost:8000/invoice_task/get_invoices.php" style="text-decoration: none;"><span class="text-success mr-2"><i class="fa fa-arrow-up"> Click here to visit</i></span></a>
                    <span class="text-nowrap"></span>
                  </p>
                </div>
              </div>
            </div>
            
          </div>
          <?php 
            if(isset($_SESSION['error_msg'])){
              ?>
              <span style="margin-left:14px;width:400px;color:white;" class="alert alert-danger fa fa-times"><?php if(isset($_SESSION['error_msg'])){ echo "    ".$_SESSION['error_msg']; }?></span>
              <?php
              }
               
              if(isset($_SESSION['add_success'])){
              ?>
              <span style="margin-left:14px;width:400px;color:white;" class="alert alert-success fa fa-check"><?php if(isset($_SESSION['add_success'])){ echo "    ".$_SESSION['add_success']; }?></span>
              <?php
              }
            ?>
        </div>
      </div>
    </div>
<?php include('./footer.php'); ?>
</body>
<?php
    if(isset($_SESSION['error_msg'])){
      unset($_SESSION['error_msg']);
    }
    if(isset($_SESSION['add_success'])){
      unset($_SESSION['add_success']);
    }
  ?>
</html>