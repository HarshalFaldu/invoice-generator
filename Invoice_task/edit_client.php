<?php
session_start();
if(!isset($_SESSION['email'])){
  header("location:dashboard.php");
}
require('./connection.php');

$email = $_GET['email'];
$decoded_email = base64_decode(urldecode($email));
if($_GET){
    $sql = 'SELECT * FROM clients WHERE email = "'.$decoded_email.'";';
    $result = $conn->query($sql);

    $row= mysqli_fetch_array($result);	

    $sql2 = 'SELECT tax_name,percentage FROM client_taxes WHERE client_id = '.$row["id"];
    $result2 = $conn->query($sql2);

    $data = [];
    while($row2 = $result2->fetch_array(MYSQLI_ASSOC)){
        $data[] = $row2;
    }
}


?>


<!DOCTYPE html>
<html>

<?php include('./head_files.php') ?>

<body class="body">
<?php include('./sidebar.php');
include('./header.php');?>

  <div class="update">
  <div class="main-content_2">
    
    <div class="container-fluid">
    
    <div class="container pt-7">
    <div class="row justify-content-center">
        <div class="col-lg-7 col-md-7">
        <div class="card bg-secondary border-0 mb-0">
            <div class="card-body px-lg-5 py-lg-5">
            <span><?php if(isset($_SESSION['error_msg'])){echo $_SESSION['error_msg'];} ?></span>
            <form action='./update_client.php?email=<?php echo$decoded_email;?>' method="POST" name="edit_client_form">
            <input type="hidden" value="<?=$row['currency']?>" name='notcurrency' id="notcurrency"  >
                <div class="form-group mb-3">
                <div class="input-group input-group-merge ">
                    <!-- <div class="input-group-prepend"> -->
                    <label for="name" class="input-group-text" style="width:130px;color:black;">Name: </label>
                    <input class="form-control"  type="text" name="name" id="name" value="<?php echo $row['name'];  ?>">
                    <!-- </div> -->
                </div>
                <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['name_error'])){echo$_SESSION['name_error'];} ?></span>
                </div>

                <div class="form-group mb-3">
                <div class="input-group input-group-merge">
                    <!-- <div class="input-group-prepend"> -->
                    <label for="email" class="input-group-text" style="width:130px;color:black;">Email: </label>
                    <!-- </div> -->
                    <input class="form-control" type="email" name="email" id="email" value="<?php echo $row['email'];  ?>">
                </div>
                <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['email_error'])){echo$_SESSION['email_error'];} ?></span>
                </div>
                
                <div class="form-group mb-3">
                <div class="input-group input-group-merge">
                    <!-- <div class="input-group-prepend"> -->
                    <label for="phone"  class="input-group-text" style="width:130px;color:black;">Phone No.: </label>
                    <!-- </div> -->
                    <input class="form-control" type="tel" id="phone" name="phone" pattern="[0-9]{10}" value="<?php echo $row['phone'];  ?>">
                </div>
                <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['phone_error'])){echo$_SESSION['phone_error'];} ?></span>
                </div>
                
                <div class="form-group mb-3">
                <div class="input-group input-group-merge">
                    <!-- <div class="input-group-prepend"> -->
                    <label for="gstin"  class="input-group-text" style="width:130px;color:black;">GSTIN: </label>
                    <!-- </div> -->
                    <input class="form-control" type="text" id="gstin" name="gstin" value="<?php echo $row['gstin'];  ?>">
                </div>
                <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['gstin_error'])){echo$_SESSION['gstin_error'];} ?></span>
                </div>

                <div class="form-group">
                <div class="input-group input-group-merge">
                <label for="address"  class="input-group-text" style="width:130px;color:black;">Address: </label>
                    <input class="form-control"  type="text" name="address" id="address" value="<?php echo $row['address'];  ?>">
                </div>
                <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['address_error'])){echo$_SESSION['address_error'];} ?></span>
                </div>
                
                <div class="form-group">
                <div class="input-group input-group-merge">
                <label for="currency"  class="input-group-text" style="width:130px;color:black;">Currency: </label>
                      <select name="currency" class="pd-currencies form-control" value="<?php echo $row['currency'];  ?>"></select>
                </div>
                <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['currency_error'])){echo$_SESSION['currency_error'];} ?></span>
                </div>

                <div class="form-group">
                <div class="input-group input-group-merge">
                    <label for="place_to_supply"  class="input-group-text" style="width:130px;color:black;">Place to supply: </label>
                    <input class="form-control" type="text" name="place_to_supply" id="place_to_supply" value="<?php echo $row['place_of_supply'];  ?>">
                </div>
                <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['place_error'])){echo$_SESSION['place_error'];} ?></span>
                </div>
                
                <div class="form-group">
                <div class="input-group input-group-merge">
                <label for="state_code"  class="input-group-text" style="width:130px;color:black;"> State code: </label>
                    <input class="form-control" type="text" name="state_code" id="state_code" value="<?php echo $row['state_code'];  ?>">
                </div>
                <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['state_error'])){echo$_SESSION['state_error'];} ?></span>
                </div>
                
                <!-- Hidden div -->
                <div class="clone_hidden"> 
                <div class="form-group edit_tax">
                <div class="input-group input-group-merge input-group-alternative">
                    <label for="tax"  class="input-group-text" style="width:70px;color:black;">Tax: </label>
                    <input class="form-control" placeholder="Tax_Name" type="text" name="tax[]" id="tax">
                    <label for="tax_rate"  class="input-group-text" style="width:90px;color:black;"> Tax rate: </label>
                    <input class="form-control" placeholder="Tax_Percentage" type="number" name="tax_rate[]" id="tax_rate" min="0" max="100">       
                    <button type="button" class="btn btn-danger  removeTax" ><i class="fa fa-trash"></i></button>        
                    <!-- <button type="button" class="btn btn-primary my-1 addTax" placeholder="Add Tax"><i class="fa fa-plus"></i></button>              -->
                </div>
                </div>
                </div>
                <div class="clone">
                <!-- Taxes -->
                <?php 
                        if($_GET and count($data) >0){
                            for($i = 0;$i<count($data);$i++){

                        ?>
                        
                        <div class="form-group edit_tax">
                        <div class="input-group input-group-merge input-group-alternative">
                            <label for="tax"  class="input-group-text" style="width:70px;color:black;">Tax: </label>
                            <input class="form-control" placeholder="Tax_Name" type="text" name="tax[]" id="tax" value="<?php echo $data[$i]['tax_name']; ?>">
                            <label for="tax_rate"  class="input-group-text" style="width:90px;color:black;"> Tax rate: </label>
                            <input class="form-control" placeholder="Tax_Percentage" type="number" name="tax_rate[]" id="tax_rate" min="0" max="100" value="<?php echo $data[$i]['percentage']; ?>">       
                            <button type="button" class="btn btn-danger  removeTax" ><i class="fa fa-trash"></i></button> 
                            <?php 
                                if($i == 0){
                            ?>
                            <button type="button" class="btn btn-primary  addTax" placeholder="Add Tax"><i class="fa fa-plus"></i></button>                    
<?php   }
                            ?>
                        </div>
                        </div>
                        <?php                             
                        }
                    }
                    ?>
                    </div>
                <div class="text-center">
                <input type="submit" class="btn btn-primary my-4" placeholder="Sign in">
                <a type="button" class="btn btn-danger my-4" href="./clients.php">Cancel</a>
                </div>
            </form>
            </div>
        </div>
        <div class="row mt-3">
    
        </div>
        </div>
    </div>
    </div>
    </div>
</div>
          </div>
    <script src="./js/pidie-0.0.8.js"></script>
  <script src="./js/main.js"></script>
  <script>
  new Pidie();

  $('form[name="edit_client_form"]').validate({
        rules: {
        name: 'required',
        email: {
            required: true,
            email: true
        },
        phone: {
            required: true,
            minlength: 10
        },
        gstin: {
            required: true,
            minlength: 15
        },
        currency: 'required',
        address: 'required',
        place_to_supply: 'required',
        state_code: {
            required: true,
            minlength: 6
          },
        },
        messages: {
        name: 'Please enter Client Name',
        email: 'Please enter a valid Email Address',
        phone: {
            required: 'Please enter a Phone No.',
            minlength: 'Your Phone no. must consist of at least 10 characters'
        },
        gstin: {
        required: 'Please enter a GST number',
        minlength: 'Your GST no. must consist of at least 15 characters'
        },
        currency: 'Please select a currency',
        address: 'Please enter client address',
        place_to_supply: 'Please enter Place to supply',
        state_code: {
            required: 'Please enter a state code number',
            minlength: 'Your state code must consist of at least 6 characters'
        },
        },
        errorElement: 'em',
        errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
    
        if (element.prop('type') === 'checkbox') {
            error.insertAfter( element.next('label'));
        } else {
            error.insertAfter(element);
        }
        },
        highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid').removeClass('is-valid');
        },
        unhighlight: function (element, errorClass, validClass) {
        $(element).addClass('is-valid').removeClass('is-invalid');
        }
    });
  </script>
 
<?php 
    unset($_SESSION['error_msg']);
    unset($_SESSION['name_error']);
    unset($_SESSION['email_error']);
    unset($_SESSION['phone_error']);
    unset($_SESSION['gstin_error']);
    unset($_SESSION['currency_error']);
    unset($_SESSION['address_error']);
    unset($_SESSION['place_error']);
    unset($_SESSION['state_error']); ?>
</body>

</html>