<?php
session_start();
if(!isset($_SESSION['email'])){
  header("location:dashboard.php");
}
require('./connection.php');
$id = $_GET['id'];
$decode_id = base64_decode(urldecode($id));
// $c_id = isset($_GET['c_id']) ? $_GET['c_id'] : null;
if($_GET){
    $sql = 'SELECT p.client_id,p.project_name,p.project_description,p.is_hourly_based,p.rate_per_hour,c.currency FROM projects p LEFT JOIN clients c on c.id = p.client_id WHERE p.id = '.$decode_id.';';
    $result = $conn->query($sql);
    @$row= mysqli_fetch_array($result);	
}



?>


<!DOCTYPE html>
<html>

<?php include('./head_files.php') ?>

<body class="body">
<?php include('./sidebar.php');
include('./header.php');?>

  <div class="update">
            <div class="main-content_2">
    
            <div class="container-fluid">
            
            <div class="container pt-7">
            <div class="row justify-content-center">
                <div class="col-lg-7 col-md-7">
                <div class="card bg-secondary border-0 mb-0">
                    <div class="card-body px-lg-5 py-lg-5">
                    <span><?php if(isset($_SESSION['error_msg'])){echo $_SESSION['error_msg'];} ?></span>
                    <form action="./update_project.php?id=<?php echo$decode_id;?>" method="POST" name="edit_project_form">

                        <div class="form-group">
                        <div class="input-group input-group-merge">
                        <label for="project_name" class="input-group-text" style="width:155px;color:black;">Project Name</label>
                            <input class="form-control" placeholder="Project Name" type="text" name="project_name" id="project_name" value="<?php echo$row['project_name'];?>">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['Pname_error'])){echo$_SESSION['Pname_error'];} ?></span>
                        </div>

                        <div class="form-group">
                        <div class="input-group input-group-merge">
                        <label for="description" class="input-group-text" style="width:155px;color:black;">Project Desc.  </label>
                            <input class="form-control" placeholder="Project Description" type="text" name="description" id="description" value="<?php echo$row['project_description'];?>">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['desc_error'])){echo$_SESSION['desc_error'];} ?></span>
                        </div>


                        <div class="form-group">
                        <div class="input-group input-group-merge">
                        <label for="is_hourly_based" class="input-group-text" style="width:155px;color:black;">Is Hourly Based </label>
                            <!-- <input class="form-control" placeholder="Place to supply" type="text" name="place_to_supply" id="place_to_supply" value="<?php //echo$row['is_hourly_based'];?>"> -->
                            <select class="form-control" name="is_hourly_based" id="is_hourly_based" class="is_hourly_based">
                            <option value="0" <?php if($row['is_hourly_based']=='0') { ?> selected="selected" <?php } ?> >No</option>
                            <option value="1" <?php if($row['is_hourly_based']=='1') { ?> selected="selected" <?php } ?> >Yes</option>
                            </select>
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['hourly_error'])){echo$_SESSION['hourly_error'];} ?></span>
                        </div>

                        <?php 
                        // echo
                            $curr = 'select currency from clients where id='.$row['client_id'];
                            $result2 = $conn->query($curr);
                            $row2= mysqli_fetch_array($result2);
                        ?>
                        <div class="form-group clone_hidden_rate">
                        <div class="input-group input-group-merge">
                        <label for="rate_per" class="input-group-text" style="width:155px;color:black;">Rate </label>
                            <input class="form-control" placeholder="<?php echo $row2['currency']; ?>" type="text" name="rate_per" id="rate_per" value="<?php echo $row['rate_per_hour'];?>">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['rate_error'])){echo$_SESSION['rate_error'];} ?></span>
                        </div>
                        
                        <div class="form-group rate">
                            
                        </div>
                       
                        </div>
                        <div class="text-center">
                            <input type="submit" class="btn btn-primary my-4" value="Update">
                            <a type="button" class="btn btn-danger my-4" href="./projects.php">Cancel</a>
                        </div>
                    </form>
                    </div>
                </div>
                <div class="row mt-3">
                    
                </div>
                </div>
            </div>
            </div>
            </div>
        </div>
          </div>
  
<?php include('./footer.php'); ?>
 <script>
 setTimeout(() => {
     $("#is_hourly_based").trigger("change");
 }, 100);

 $('form[name="edit_project_form"]').validate({
        rules: {
        project_name: 'required',
        description: 'required',
        is_hourly_based: 'required',
        },
        messages: {
        project_name: 'Please enter Project Name',
        description: 'Please enter Project Description',
        is_hourly_based: 'Please select Project type'
        },
        errorElement: 'em',
        errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
    
        if (element.prop('type') === 'checkbox') {
            error.insertAfter( element.next('label'));
        } else {
            error.insertAfter(element);
        }
        },
        highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid').removeClass('is-valid');
        },
        unhighlight: function (element, errorClass, validClass) {
        $(element).addClass('is-valid').removeClass('is-invalid');
        }
    });
</script>
 <?php
    unset($_SESSION['Pname_error']);
    unset($_SESSION['desc_error']);
    unset($_SESSION['hourly_error']);
    unset($_SESSION['rate_error']);
    unset($_SESSION['error_msg']);

?>

</body>

</html>

