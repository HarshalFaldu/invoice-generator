<?php
session_start();
if(!isset($_SESSION['email'])){
  header("location:dashboard.php");
}
require('./connection.php');

$sql = 'SELECT * FROM users;';
$result = $conn->query($sql);
$row= mysqli_fetch_array($result);	

?>


<!DOCTYPE html>
<html>

<?php include('./head_files.php') ?>

<body class="body">
<?php include('./sidebar.php');
include('./header.php');?>

  <div class="update">
            <div class="main-content_2">
    
            <div class="container-fluid">
            
            <div class="container pt-7">
            <div class="row justify-content-center">
                <div class="col-lg-7 col-md-7">
                <div class="card bg-secondary border-0 mb-0">
                    <div class="card-body px-lg-5 py-lg-5">
                    <?php 
                    if(isset($_SESSION['error_msg'])){
                    ?>
                    <span style="margin-left:14px;width:400px;color:white;" class="alert alert-danger fa fa-times"><?php if(isset($_SESSION['error_msg'])){ echo "    ".$_SESSION['error_msg']; }?></span>
                    <?php
                    }
                    if(isset($_SESSION['add_success'])){
                    ?>
                    <span style="margin-left:14px;width:400px;color:white;" class="alert alert-success fa fa-check"><?php if(isset($_SESSION['add_success'])){ echo "    ".$_SESSION['add_success']; }?></span>
                    <?php
                    }
                    ?>
                    <form action="./update_user.php?id=<?php echo$row['id'];?>" method="POST">

                        <div class="form-group">
                        <div class="input-group input-group-merge">
                        <label for="name" class="input-group-text" style="width:155px;color:black;">Name</label>
                            <input class="form-control" placeholder="Project Name" type="text" name="name" id="name" value="<?php echo $row['name'];?>">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['Uname_error'])){echo$_SESSION['Uname_error'];} ?></span>
                        </div>

                        <div class="form-group">
                        <div class="input-group input-group-merge">
                        <label for="DOB" class="input-group-text" style="width:155px;color:black;">DOB.  </label>
                            <input class="form-control" placeholder="Project Description" type="text" name="DOB" id="DOB" value="<?php echo$row['DOB'];?>">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['dob_error'])){echo$_SESSION['dob_error'];} ?></span>
                        </div>
                        
                        <div class="form-group">
                        <div class="input-group input-group-merge">
                        <label for="mobile" class="input-group-text" style="width:155px;color:black;">Mobile No.  </label>
                            <input class="form-control" placeholder="Project Description" type="number" name="mobile" id="mobile" value="<?php echo$row['mobile'];?>">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['mobile_error'])){echo$_SESSION['mobile_error'];} ?></span>
                        </div>
                        
                        <div class="form-group">
                        <div class="input-group input-group-merge">
                        <label for="email" class="input-group-text" style="width:155px;color:black;">Email Id.  </label>
                            <input class="form-control" placeholder="Project Description" type="text" name="email" id="email" value="<?php echo$row['email'];?>">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['email_error'])){echo$_SESSION['email_error'];} ?></span>
                        </div>

                        <div class="hiddendiv">
                            <div class="form-group">
                            <div class="input-group input-group-merge">
                            <label for="" class="input-group-text" style="width:155px;color:black;"></label>
                                <input class="form-control" type="password" name="" id="" >
                            </div>
                            </div>
                        </div>

                        <div class="pass">

                        </div>


                        <div class="row mt-3">
                        <div class="col-6">
                        <a class="text-light changep" type="button"><small>Change Password</small></a>
                        </div>
                        </div>
                       
                        </div>
                        <div class="text-center">
                            <input type="submit" class="btn btn-primary my-4" value="Update">
                            <a type="button" class="btn btn-danger my-4" href="./dashboard.php">Cancel</a>
                        </div>
                    </form>
                    </div>
                </div>
                <div class="row mt-3">
                    
                </div>
                </div>
            </div>
            </div>
            </div>
        </div>
          </div>
  
<?php include('./footer.php'); ?>
 <script>
 setTimeout(() => {
     $("#is_hourly_based").trigger("change");
 }, 100);
</script>
 <?php

    unset($_SESSION['error_msg']);
    unset($_SESSION['add_success']);

?>

</body>

</html>

