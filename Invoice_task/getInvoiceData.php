<?php
require('./connection.php');
if($_POST['operation'] == "invoice"){
    $sql = 'select i.id,i.invoice_date,i.due_date,i.client_id,i.amount,c.currency,i.status,GROUP_CONCAT(" ",p.project_name) as project_id from invoices as i LEFT JOIN invoice_items as it on i.id = it.invoice_id LEFT JOIN projects p on p.id = it.project_id LEFT JOIN clients c on c.id = i.client_id where i.is_deleted="active" group by it.invoice_id';
    $result = $conn->query($sql);
    
    $data = [];
    while($row = $result->fetch_array(MYSQLI_ASSOC)){
        $data[] = $row;
    }

    $data2 = [];
    for($i = 0;$i<count($data);$i++){
        $sql2 = 'select email from clients where id = '.$data[$i]['client_id'];
        $result2 = $conn->query($sql2);
        while($row2 = $result2->fetch_array(MYSQLI_ASSOC)){
            $data2[] = $row2;
        }
    }

    $results = ["success" => 1,
                "data" => $data,
                "client_name" => $data2,
                "messgae" => "Success" ];

    echo json_encode($results);
}

if($_POST['operation']=="delete"){
    $sql = 'update invoices set is_deleted = "inactive" where id='.$_POST['id'];
    $result = $conn->query($sql);

    if(!$result){
        $message = "Deletion Failed";
        $_SESSION['error_in_adding'] = $message;
    }else{
        $message = "Successfully deleted";
        $_SESSION['add_success'] = $message;
    }

    $results = ["success" => 1,
            "message" => $message ];
    echo json_encode($results);
}

if($_POST['operation'] == "removed_invoice"){
    $sql = 'select i.id,i.invoice_date,i.due_date,i.client_id,i.amount,c.currency,i.status,GROUP_CONCAT(it.project_id," ") as project_id from invoices as i LEFT JOIN invoice_items as it on i.id = it.invoice_id LEFT JOIN clients c on c.id = i.client_id where i.is_deleted="inactive" group by it.invoice_id';
    $result = $conn->query($sql);
    
    $data = [];
    while($row = $result->fetch_array(MYSQLI_ASSOC)){
        $data[] = $row;
    }

    $data2 = [];
    for($i = 0;$i<count($data);$i++){
        $sql2 = 'select email from clients where id = '.$data[$i]['client_id'];
        $result2 = $conn->query($sql2);
        while($row2 = $result2->fetch_array(MYSQLI_ASSOC)){
            $data2[] = $row2;
        }
    }

    $results = ["success" => 1,
                "data" => $data,
                "client_name" => $data2,
                "messgae" => "Success" ];

    echo json_encode($results);
}

?>