<?php
    
    require('./connection.php');

    if($_POST['operation']=="get_client"){

     $sql = "SELECT c.*,if(GROUP_CONCAT(t.tax_name,'=>',t.percentage) IS null,0,GROUP_CONCAT(' ',t.tax_name,'=>',t.percentage)) as tax_included FROM clients as c left JOIN client_taxes as t ON c.id = t.client_id where is_deleted = 'active' group by c.id";
     $result = $conn->query($sql);
 
     while($row = $result->fetch_array(MYSQLI_ASSOC)){
         $data[] = $row;
     }
     $encoded_mail = [];
     for($i = 0;$i<count($data);$i++){
        $encoded_mail[] = urlencode(base64_encode($data[$i]['email']));
     }
     $results = ["success" => 1,
                "data" => $data,
                "encode" => $encoded_mail, 
                "messgae" => "Success" ];
     
    }
    if($_POST['operation']=="get_removed_client"){
      $sql = "SELECT c.*,if(GROUP_CONCAT(t.tax_name,'=>',t.percentage) IS null,0,GROUP_CONCAT(' ',t.tax_name,'=>',t.percentage)) as tax_included FROM clients as c left JOIN client_taxes as t ON c.id = t.client_id where is_deleted = 'inactive' group by c.id";
      $result = $conn->query($sql);
  
      while($row = $result->fetch_array(MYSQLI_ASSOC)){
          $data[] = $row;
      }
      $results = ["success" => 1,
                  "data" => $data,
                  "messgae" => "Success" ];
    }

    



    echo json_encode($results);
?>