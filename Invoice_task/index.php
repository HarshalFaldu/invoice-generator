<?php

session_start();
if(!isset($_SESSION['email'])){
  header("location:dashboard.php");
}
require('./connection.php');
$sql = 'select * from company_details;';
$result = $conn->query($sql);
$row= mysqli_fetch_array($result);

$sql2 = 'select max(id) as id from invoices;';
$result2 = $conn->query($sql2);
$row2= mysqli_fetch_array($result2);

?>


<!DOCTYPE html>
<html>

<?php include('./head_files.php') ?>

<body class="body" style="max-width:1099px;">
  <!-- Sidenav -->
  <?php include('./sidebar.php');
include('./header.php');?>




  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    
    <!-- Header -->
    <!-- Header -->
    <div class="header  pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 d-inline-block mb-0">Create Invoice</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Create Invoice</li>
                </ol>
              </nav>
            </div>
            <!-- <div class="col-lg-6 col-5 text-right">
              <a href="#" class="btn btn-neutral">Add New Client</a>
            </div> -->
          </div>
          <!-- Row1-->
          <!-- LOGO -->
          <h2 class="heading text-muted mb-4" style="text-align: center;font-size: 30px;letter-spacing: 4px;">Create Invoice</h2>
          <hr class="my-4">
           
           <h6 class="heading text-muted mb-5" style="text-align: center;">Invoice Details</h6>

          <form action="./invoice_save.php" method="POST">
            
            <!-- Company number details -->

            <table class="mb-5">
              <td>
                <table>
                  <tr>
                  <td><table>
                <tr>
                <div class=" mb-3" style="display:flex;align-items:baseline;">
                   <label for="PAN" style="width: 220px;display:inline;">Select Client:</label>
                   <div class="input-group-merge" style="width:250px;">
                      <select  class="form-control" name="clientId" id="clientId" required maxlength="10">
                      <option value="" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;max-width: 100px;">None</option>
                      <?php
                      require('./connection.php');
                      $select_client = 'select * from clients where is_deleted="active";';
                      $run = $conn->query($select_client);
                      print_r(mysqli_error($conn));
                      if ($run->num_rows > 0) {
                          while($options = $run->fetch_assoc()) { ?>
                              <option value="<?php echo $options['id'];?>" ><?php echo $options['name']."(". $options['email'].")";?></option>
                              <?php
                          }
                      }

                      ?>
                      </select>
                      </div>
                  </div>
                </tr>
                </table></td>
                  </tr>
                  <tr>
                    <td>
                      <div class=" mb-3" style="display:flex;align-items:baseline;">
                          <label for="clientAddress" style="width: 220px;display:inline;">Client's Address:</label>
                          <div class="input-group-merge" style="width:250px;">
                          <input class="form-control" type="text" name="clientAddress" id="clientAddress" style="display:inline;" >
                          </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div class=" mb-3" style="display:flex;align-items:baseline;">
                          <label for="clientPlace_of_supply" style="width: 220px;display:inline;">Place to Supply:</label>
                          <div class=" input-group-merge" style="width:250px;">
                          <input class="form-control" type="text" name="clientPlace_of_supply" id="clientPlace_of_supply" style="display:inline;" >
                          </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div class=" mb-3" style="display:flex;align-items:baseline;">
                          <label for="clientState_code" style="width: 220px;display:inline;">State Code:</label>
                          <div class=" input-group-merge" style="width:250px;">
                          <input class="form-control" type="text" name="clientState_code" id="clientState_code" style="display:inline;" >
                          </div>
                      </div>
                    </td>
                  </tr>
                </table>
              </td>
              <td   valign="top">
                <table style="margin-left:30px;">
                  <tr>
                    <td>
                      <div class=" mb-3" style="display:flex;align-items:baseline;">
                          <label for="inNO" style="width: 220px;display:inline;">Invoice No:</label>
                          <div class=" input-group-merge" style="width:250px;">
                          <input class="form-control" type="text" name="inNO" id="inNO" style="display:inline;" value="<?php echo ($row2['id']+1); ?>">
                          </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div class=" mb-3" style="display:flex;align-items:baseline;">
                          <label for="indt" style="width: 220px;display:inline;">Invoice Date</label>
                          <div class=" input-group-merge" style="width:250px;">
                          <input class="form-control" placeholder="Name" type="date" name="indt" id="indt" style="display:inline;" required>
                          </div>
                          <span style="color:red;font-size:12px;"></span>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div class=" mb-3" style="display:flex;align-items:baseline;">
                          <label for="dudt" style="width: 220px;display:inline;">Due Date</label>
                          <div class=" input-group-merge" style="width:250px;">
                          <input class="form-control" placeholder="Name" type="date" name="dudt" id="dudt" style="display:inline;" required>
                          </div>
                          <span style="color:red;font-size:12px;"></span>
                      </div>
                    </td>
                  </tr>
                  <tr>
                  <td>
                      <div class=" mb-3" style="display:flex;align-items:baseline;">
                          <!-- <label for="dudt" style="width: 220px;display:inline;">Due Date</label> -->
                          <div class=" input-group-merge" style="width:250px;">
                          <input class="form-control" placeholder="Name" type="date" style="display:none;">
                          </div>
                          <!-- <span style="color:red;font-size:12px;"></span> -->
                      </div>
                    </td>
                  </tr>
                </table>
              </td>
            </table>

            <hr class="my-4">
           
            <h6 class="heading text-muted mb-5" style="text-align: center;">Project Details</h6>

            <!-- Billing Table -->
            <div class="form-group mb-3">
            <table style="width: 100%;text-align:center;" class="bills" >
                <tr >
                  <!-- <th style="width: 16.66%;">No.</th> -->
                  <th style="width: 16.66%;padding-bottom:20px;">Project Name</th>
                  <th style="width: 16.66%;padding-bottom:20px;">Project Description</th>
                  <th style="width: 16.66%;padding-bottom:20px;">IS Hourly based</th>
                  <th style="width: 16.66%;padding-bottom:20px;">Hours</th> 
                  <th style="width: 16.66%;padding-bottom:20px;">Rate</th>
                  <th style="width: 16.66%;padding-bottom:20px;">Total</th>
                </tr>
                <tr >
                  <td>
                      <table>
                        <tr >
                          <div class="form-group">
                          <div class="input-group " >
                            <select  class="form-control" name="projectSelect[]" id="projectSelect" required style="margin: 0 0 0 10px;">
                            <input type="hidden" name="hiddenField" id="hiddenField" value="" /> 
                            

                            </select>
                          </div>
                          </div>
                        </tr>
                      </table>
                  </td>
                  <td>
                    <div class="form-group">
                    <div class="input-group ">
                        <div class="input-group-prepend">
                        <!-- <span class="input-group-text"><i class="fa fa-address-card"></i></span> -->
                        </div>
                        <input class="form-control"  type="text" name="projectDescription[]" id="projectDescription" style="margin: 0 0 0 10px;">
                        <input type="hidden" name="hiddenField2[]" id="hiddenField2"> 
                    </div>
                    </div>
                  </td>
                  <td>
                    <div class="form-group">
                    <div class="input-group ">
                        <div class="input-group-prepend">
                        <!-- <span class="input-group-text"><i class="fa fa-address-card"></i></span> -->
                        </div>
                        <input class="form-control"  type="text" name="project_is_hourly_based[]" id="project_is_hourly_based" style="margin: 0 0 0 10px;">
                    </div>
                    
                    </div>
                  </td>
                  <td>
                    <div class="form-group">
                    <div class="input-group  ">
                        <div class="input-group-prepend">
                        <!-- <span class="input-group-text"><i class="fa fa-address-card"></i></span> -->
                        </div>
                        <input class="form-control"  type="text" name="project_hours[]" id="project_hours" style="margin: 0 0 0 10px;">
                    </div>
                    </div>
                  </td>
                  
                  <td>
                    <div class="form-group">
                    <div class="input-group ">
                        <div class="input-group-prepend">
                        <!-- <span class="input-group-text"><i class="fa fa-address-card"></i></span> -->
                        </div>
                        <input class="form-control" type="text" name="rate[]" id="rate" style="margin: 0 0 0 10px;">
                    </div>
                    </div>
                  </td>
                  <td>
                    <div class="form-group">
                    <div class="input-group ">
                        <div class="input-group-prepend">
                        <!-- <span class="input-group-text"><i class="fa fa-address-card"></i></span> -->
                        </div>
                        <input class="form-control update_price" type="text" name="update_price[]" id="update_price" style="margin: 0 0 0 10px;">
                    </div>
                    </div>
                  </td>
                </tr>
                
            </table>
            <button type="button" class="btn btn-primary my-4 addpros">Add Project</button>
            </div>

           
            
            <div class="text-center">
              <input type="submit" class="btn btn-success my-4" name="save_download" id="save_download" value="Save and Download" >
              <button type="submit" class="btn btn-info my-4" name="Download" id="Download">Download</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">

    </div>
  </div>
  <?php include('./footer.php'); ?>
  <script>
    setTimeout(() => {
      var now = new Date();
      if(now.getMonth() + 1 < 10){
        month = "0"+(now.getMonth()+1)
      }else{
        month = (now.getMonth()+1)
      }
      var today = now.getFullYear()  + '-' + (month) + '-' + now.getDate();
      $("#indt").val(today)
    }, 100);

    $(document).on("change","#dudt",function(){
      due = $(this).val();
      currDate = $("#indt").val()
      if(due < currDate){
        alert("Due date can't be less than current date");
        due = $(this).val("");
      }
    })
  </script>
</body>

</html> 