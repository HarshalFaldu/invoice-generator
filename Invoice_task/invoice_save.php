<?php

session_start();
if(!isset($_SESSION['email'])){
  header("location:dashboard.php");
}
require('./connection.php');
$sql = 'select * from company_details where id=1';
$result = $conn->query($sql);
$row = mysqli_fetch_assoc($result);

$sql2 = 'select * from clients where id='.$_POST['clientId'];
$result2 = $conn->query($sql2);
$row2 = mysqli_fetch_assoc($result2);

if(isset($_POST['save_download'])){
    include('./save_and_download.php');
}

// $row['company_address'] = str_replace(",",",<br>",$row['company_address']);

require('./TCPDF/tcpdf.php');

// create new PDF document
$tcpdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set margins
$tcpdf->SetMargins(5, 12, 5, true);
$tcpdf->AddPage('P', 'A4');

$pdf = '<table border="1" style="width:100%;">
            <tbody>
                <tr>
                    <td  rowspan="2" ></td>
                    <td colspan="4" style="height:50px;font-size:24px;text-align:center;line-height:45px;">'.$row['company_full_title'].'</td>
                    <td style="font-size:22px;text-align:center;line-height:45px;">Invoice</td>   
                </tr>
                <tr>
                    <td colspan="2" style="height:50px;font-size:18px;text-align:center;line-height:45px;"> Web : <span style="color:blue;">'.$row['website_address'].'</span></td>
                    <td colspan="2" style="height:50px;font-size:18px;text-align:center;line-height:45px;"> Mail : <span style="color:blue;">'.$row['mail_address'].'</span></td>
                    <td style="font-size:18px;text-align:center;line-height:45px;">TAX Invoice</td>   
                </tr>
            </tbody>
            </table>
            <div></div>

        <table border="1" style="border-collapse: collapse;"  CELLPADDING= "5">
            <tbody>
            <tr>
            <td style="font-size:16px;background-color:lightgrey;">Company Address </td>
            </tr>
                <tr style="">
                    <td rowspan="4" style="width:245px;word-wrap:break-word;font-size:16px;">'.$row['company_address'].'</td>
                </tr>
            </tbody>
        </table>
        <div></div>
        <table border="1" style="border-collapse: collapse;font-size:14px;" CELLPADDING= "4">
            <tbody>
                <tr>
                <td style="background-color:lightgrey;">CIN Number : </td>
                <td>'.$row['CIN_number'].'</td>
                <td style="background-color:lightgrey;">Invoice No. : </td>
                <td>'.$_POST['inNO'].'</td>
                </tr>
                <tr>
                <td style="background-color:lightgrey;">Company PAN Number : </td>
                <td>'.$row['PAN_number'].'</td>
                <td style="background-color:lightgrey;">Invoice Date : </td>
                <td>'.$_POST['indt'].'</td>
                </tr>
                <tr>
                <td style="background-color:lightgrey;">Company GST Number : </td>
                <td>'.$row['GST_number'].'</td>
                <td style="background-color:lightgrey;">Due Date : </td>
                <td>'.$_POST['dudt'].'</td>
                </tr>
                <tr>
                <td style="background-color:lightgrey;">Company I.E.C. : </td>
                <td>'.$row['IEC_number'].'</td>
                <td></td>
                <td></td>
                </tr>
                
            </tbody>
        </table>
        <div></div>
        <table border="1" CELLPADDING="4">
            <tbody>
                <tr style="background-color:lightgrey;font-size:16px">
                    <th>Bill To</th>
                    <th style = "width:176px;">Address</th>
                    <th>Place to Supply</th>
                    <th>State Code</th>
                </tr>

                <tr style="font-size:14px;height:100px;">
                    <td>'.$row2['name'].'<br><span style="font-size:12px;">'.$row2['email'].'</span><br><span>GSTIN:</span><br>'.$row['GST_number'].'</td>
                    <td style="word-wrap:break-word;">'.$_POST['clientAddress'].'</td>
                    <td>'.$_POST['clientPlace_of_supply'].'</td>
                    <td>'.$_POST['clientState_code'].'</td>
                </tr>
            </tbody>
        </table>
        <div></div>
        <table border="1" CELLPADDING="3">
            <tbody>
                <tr style="background-color:lightgrey;">
                    <th style = "width:60px;">No.</th>
                    <th style = "width:80px;">SAC/HSN</th>
                    <th style = "width:300px;">Project Description</th>
                    <th style = "width:70px;">Hours</th>
                    <th style = "width:81px;">Rate / HR</th>
                    <th>Amount</th>
                </tr>';

for($i = 0;$i<count($_POST['projectSelect']);$i++){
    $pdf .= '<tr><td>'.($i+1).'</td>';
    $pdf .= '<td>'.$row['HSN'].'</td>';
    $pdf .= '<td>'.$_POST['projectDescription'][$i].'</td>';
    $pdf .= '<td>'.$_POST['project_hours'][$i].'</td>';
    // $pdf .= '<td>'.$_POST['project_is_hourly_based'][$i].'</td>';
    $pdf .= '<td>'.$_POST['rate'][$i].' '.$_POST["hiddenField"].'</td>';
    $pdf .= '<td>'.$_POST['update_price'][$i].'</td></tr>';
}
for($j = 0;$j <= count($_POST['ClientTax']);$j++){
    if($j != count($_POST['ClientTax'])){
        $pdf .= '<tr><td></td>';
        $pdf .= '<td></td>';
        $pdf .= '<td></td>';
        // $pdf .= '<td></td>';
        $pdf .= '<td>'.$_POST['ClientTax'][$j].'</td>';
        $pdf .= '<td>'.$_POST['clientPercentage'][$j].' %</td>';
        $pdf .= '<td>'.$_POST['update_price'][$j+count($_POST['projectSelect'])].'</td></tr>';
    }else{
        // $pdf .= '<td></td>';
        $pdf .= '<tr><td style="font-weight:25px;background-color:lightgrey;" colspan="5"><b>Total Invoice Amount</b></td>';
        $pdf .= '<td style="background-color:lightgrey;">'.$_POST['No'].'</td></tr>';
    }
}
$pdf .= '<tr><td colspan="7" style="background-color:lightgrey;"><b>Total Amount in Words: </b>'.numberTowords($_POST['No']).'</td><td></td><td></td><td></td><td></td><td></td></tr>';
$pdf .= '<tr><td colspan="7" style="background-color:lightgrey;"><b>Payment Milestone:</b> Monthly Payment Upon Invoice</td><td></td><td></td><td></td><td></td><td></td></tr>';
$pdf .= '</tbody></table>';

$pdf .= '<div></div><table border="1" CELLPADDING="4">
            <tbody>
                <tr>
                    <td colspan="4" style="background-color:lightgrey;">Bank Details: '.$row['company_full_title'].'</td>
                </tr>
                <tr>
                    <td>Bank Name : </td>
                    <td>'.$row['bank_name'].'</td>
                    <td style="font-size:16px;" colspan="2">Notes : </td>
                </tr>
                <tr>
                    <td>INR Account No. : </td>
                    <td>'.$row['INR_account_number'].'</td>
                    <td colspan="2">'.$row['note1'].'</td>
                </tr>
                <tr>
                    <td>EURO Account No. : </td>
                    <td>'.$row['EURO_account_number'].'</td>
                    <td colspan="2">'.$row['note2'].'</td>
                </tr>
                <tr>
                    <td>Routing Number : </td>
                    <td>'.$row['routing_number'].'</td>
                    <td colspan="2">'.$row['note3'].'</td>
                </tr>
                <tr>
                    <td>Swift Code : </td>
                    <td>'.$row['swift_code'].'</td>
                    <td colspan="2">'.$row['note4'].'</td>
                </tr>
                <tr>
                    <td>Mobile Number : </td>
                    <td>'.$row['mobile_number'].'</td>
                    <td colspan="2">'.$row['note5'].'</td>
                </tr>
            </tbody>

</table><div></div>';

$pdf .= '<table border="1" CELLPADDING="5"  style="text-align:center;">
            <tbody>
                <tr>
                    <td colspan="2" style="line-height:34px;">'.$row['thank_you_message'].'<br><span>'.$row['first_name'].' '.$row['last_name'].' | Director</span></td>
                    <td colspan="2"></td>
                </tr>
                
            </tbody>

</table>';

// set default monospaced font
$tcpdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set title of pdf
$tcpdf->SetTitle('WebOccult Certificate');



// set header and footer in pdf
$tcpdf->setPrintHeader(false);
$tcpdf->setPrintFooter(false);
$tcpdf->setListIndentWidth(3);

// set auto page breaks
$tcpdf->SetAutoPageBreak(TRUE, 11);

// set image scale factor
$tcpdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$tcpdf->SetFont('times', '', 11);

$tcpdf->writeHTML($pdf, true, false, false, false, '');



$tcpdf->Image('./assets/images/logo.png', 7, 12.5, 27, 27, 'png','', '', true, 200, '', false, false, 0, false, false, true);

//Close and output PDF document
ob_end_clean();
$tcpdf->Output('demo.pdf', 'I');



// Reference from https://www.nicesnippets.com/blog/how-to-convert-number-to-words-using-php
function numberTowords(float $amount)
{
   $amount_after_decimal = round($amount - ($num = floor($amount)), 2) * 100;
   // Check if there is any number after decimal
   $amt_hundred = null;
   $count_length = strlen($num);
   $x = 0;
   $string = array();
   $change_words = array(0 => '', 1 => 'One', 2 => 'Two',
     3 => 'Three', 4 => 'Four', 5 => 'Five', 6 => 'Six',
     7 => 'Seven', 8 => 'Eight', 9 => 'Nine',
     10 => 'Ten', 11 => 'Eleven', 12 => 'Twelve',
     13 => 'Thirteen', 14 => 'Fourteen', 15 => 'Fifteen',
     16 => 'Sixteen', 17 => 'Seventeen', 18 => 'Eighteen',
     19 => 'Nineteen', 20 => 'Twenty', 30 => 'Thirty',
     40 => 'Forty', 50 => 'Fifty', 60 => 'Sixty',
     70 => 'Seventy', 80 => 'Eighty', 90 => 'Ninety');
  $here_digits = array('', 'Hundred','Thousand','Lakh', 'Crore');
  while( $x < $count_length ) {
       $get_divider = ($x == 2) ? 10 : 100;
       $amount = floor($num % $get_divider);
       $num = floor($num / $get_divider);
       $x += $get_divider == 10 ? 1 : 2;
       if ($amount) {
         $add_plural = (($counter = count($string)) && $amount > 9) ? 's' : null;
         $amt_hundred = ($counter == 1 && $string[0]) ? ' and ' : null;
         $string [] = ($amount < 21) ? $change_words[$amount].' '. $here_digits[$counter]. $add_plural.' 
         '.$amt_hundred:$change_words[floor($amount / 10) * 10].' '.$change_words[$amount % 10]. ' 
         '.$here_digits[$counter].$add_plural.' '.$amt_hundred;
         }else $string[] = null;
       }
   $implode_to_Rupees = implode('', array_reverse($string));
   $get_paise = ($amount_after_decimal > 0) ? "And " . ($change_words[$amount_after_decimal / 10] . " 
   " . $change_words[$amount_after_decimal % 10]) . ' Paise' : '';
   return ($implode_to_Rupees ? $implode_to_Rupees . ' ' : '') . $get_paise;
}





?>
