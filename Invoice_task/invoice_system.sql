-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 22, 2021 at 09:45 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `invoice_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` bigint(20) NOT NULL,
  `gstin` varchar(15) NOT NULL,
  `currency` varchar(5) NOT NULL,
  `address` varchar(200) NOT NULL,
  `place_of_supply` varchar(50) NOT NULL,
  `state_code` varchar(50) NOT NULL,
  `is_deleted` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `client_taxes`
--

CREATE TABLE `client_taxes` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `tax_name` varchar(20) NOT NULL,
  `percentage` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_details`
--

CREATE TABLE `company_details` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `company_full_title` varchar(60) NOT NULL,
  `website_address` varchar(200) NOT NULL,
  `mail_address` varchar(100) NOT NULL,
  `company_address` varchar(200) NOT NULL,
  `CIN_number` varchar(21) NOT NULL,
  `PAN_number` varchar(10) NOT NULL,
  `GST_number` varchar(15) NOT NULL,
  `IEC_number` varchar(10) NOT NULL,
  `HSN` int(11) NOT NULL,
  `bank_name` varchar(50) NOT NULL,
  `INR_account_number` bigint(14) NOT NULL,
  `EURO_account_number` bigint(14) NOT NULL,
  `routing_number` int(9) NOT NULL,
  `swift_code` varchar(11) NOT NULL,
  `mobile_number` bigint(10) NOT NULL,
  `note1` varchar(100) NOT NULL,
  `note2` varchar(100) NOT NULL,
  `note3` varchar(100) NOT NULL,
  `note4` varchar(100) NOT NULL,
  `note5` varchar(100) NOT NULL,
  `thank_you_message` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_details`
--

INSERT INTO `company_details` (`id`, `first_name`, `last_name`, `company_full_title`, `website_address`, `mail_address`, `company_address`, `CIN_number`, `PAN_number`, `GST_number`, `IEC_number`, `HSN`, `bank_name`, `INR_account_number`, `EURO_account_number`, `routing_number`, `swift_code`, `mobile_number`, `note1`, `note2`, `note3`, `note4`, `note5`, `thank_you_message`) VALUES
(1, '  Disha', ' Joshi', '  Weboccult Technologies', '  www.weboccult.com', 'info@weboccult.com', '402/403-502/503 Akik Tower,Near Pakwan Cross Road, S.G. Highway, Bodakdev, Ahmedabad - 3800154, Contact No: +917948003449', ' U72900GJ2019PTC1', ' AACCW', ' 24AACCW421', ' AACCW', 998314, ' HDFC Bank', 50200039345666, 50200040248555, 26009593, ' HDFCIN', 9033542002, ' Amount of the bill to be paid before due date', ' Subject to Jurisdiction Ahmedabad, Gujarat-India Only', ' Pay only mentioned currency in Invoice', '     ', '     ', ' Thank You for your Business');

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int(11) NOT NULL,
  `invoice_date` date NOT NULL,
  `due_date` date NOT NULL,
  `client_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `is_deleted` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_items`
--

CREATE TABLE `invoice_items` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `hours` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `insert_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `project_name` varchar(100) NOT NULL,
  `project_description` varchar(255) NOT NULL,
  `is_hourly_based` int(11) NOT NULL DEFAULT 0,
  `rate_per_hour` int(11) NOT NULL,
  `is_deleted` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `DOB` date NOT NULL,
  `mobile` bigint(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `insert_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `DOB`, `mobile`, `email`, `password`, `insert_time`) VALUES
(1, 'Disha Joshi ', '1990-05-10', 9924466822, 'weboccult@gmail.com', 'ab54d425fa8ac654e8cdf0307eebcc33ea46d08e', '2021-05-22 19:21:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `client_taxes`
--
ALTER TABLE `client_taxes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_details`
--
ALTER TABLE `company_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_items`
--
ALTER TABLE `invoice_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `client_taxes`
--
ALTER TABLE `client_taxes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company_details`
--
ALTER TABLE `company_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_items`
--
ALTER TABLE `invoice_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
