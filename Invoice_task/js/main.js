$(document).ready(function(){

    //Add projects in invoice
    var row = 1
    $(document).on("click",".addpros",function(){
        var bill = $(".bills").find('tr').eq(1).html();
        $(".bills tr:eq("+row+")").after('<tr>'+bill+'</tr>');
        row++;
    })

    var selectedClientCurrency = "";
    //Dropdown for logout and Edit profile
    var tax = $(".clone_hidden").html();
    $(".profile-dropdown").click(function(){
        $(".dropdown-menu").toggleClass("active-drop");
    });
    
    // Add client details as per Selection in dropdown
    var client_currency
    $(document).on("change","#clientId",function(){
        // $(".bills").reset();
        $(".bills").find("tr:gt(1)").remove();
        var val = $("#clientId").val();
        $.ajax({
            url: "./getClient.php",
            method: "POST",
            dataType : "JSON",
            data : { clientID : val },
            success: function(response){
                $("#projectDescription").val("");
                $("#project_hours").val("");
                $("#project_is_hourly_based").val("");
                $("#update_price").val("");
                $("#rate").val("");
                selectedClientCurrency =response.data[0].currency; 
                $("#clientAddress").val(response.data[0].address)
                $("#clientPlace_of_supply").val(response.data[0].place_of_supply)
                $("#clientState_code").val(response.data[0].state_code)
                $("#hiddenField").val(selectedClientCurrency);
                var op = '<option value="None" selected>None</option>'
                for(var i = 0;i<response.data2.length;i++){
                    op += '<option value = "'+response.data2[i].project_name+'">'+response.data2[i].project_name+'</option>'
                }
                $("#projectSelect").html(op);
                op = "";
            }
        });
        setTimeout(() => {
            amount= 0
            $.ajax({
                url: "./getProject.php",
                method: "POST",
                dataType : "JSON",
                data : { clientID : val, operation:"tax" },
                success:function(response){
                    client_currency = response.data[0].currency;
                    for(var i = 0;i<=response.data.length;i++){
                        var st = '<tr>'
                        var middle = `<td><div class="form-group"><div class="input-group"><div class="input-group-prepend">
                        </div>
                          <input class="form-control"  type="text" name="No" id="No" value="" style="margin: 0 0 0 10px;">
                        </div>
                      </div></td>`
                      if(i!=response.data.length){
                          
                          var middletax = `<td><div class="form-group"><div class="input-group"><div class="input-group-prepend">
                          </div>
                            <input class="form-control"  type="text" name="ClientTax[]" id="ClientTax" value="${response.data[i].tax_name}" style="margin: 0 0 0 10px;">
                          </div>
                        </div></td>`
                          var middleperc = `<td><div class="form-group"><div class="input-group "><div class="input-group-prepend">
                          </div>
                            <input class="form-control clientPercentage"  type="text" name="clientPercentage[]" id="clientPercentage" value="${response.data[i].percentage}" style="margin: 0 0 0 10px;">
                          </div>
                        </div></td>`
                        
                          for(var j = 1; j<=6;j++){
                              if(j==4){
                                  st += middletax
                              }else if(j==5){
                                  st+=middleperc
                              }else if(j==6){
                                  var mid = middle.replace('class="form-control"','class="form-control update_price"')
                                  mid = mid.replace('name="No"','name="update_price[]"')
                                  mid = mid.replace('value=""','value="'+(response.data[i].percentage*amount)/100+" "+client_currency+'"');
                                  st+=mid
                              }else{
                                  st+=middle
                              }
                          }
                      }else{
                        for(var k = 1; k<=6;k++){
                            if(k==1){
                                var mid = middle.replace('class="form-control"','class="form-control"')
                                  mid = mid.replace('value','value="Total"')
                                  st+=mid
                            }
                            else if(k==6){
                                var mid = middle.replace('class="form-control"','class="form-control totalPrice"')
                                  mid = mid.replace('value=""','value="0 '+client_currency+'"')
                                  st+=mid
                            }else{
                                st+=middle
                            }
                        }
                      }
                        st+="</tr>";
                        $(".bills").append(st);
                        st = "";
                    }
                }
               
            })
        }, 100);
    })

// Select project details as per value selected in dropdown
    var RealPrice = []; 
    $(document).on("change","#projectSelect",function(){
        var vall = $(this).val();
        var c_id = $("#clientId").val();
        var ths = $(this);
        $.ajax({
            url: "./getProject.php",
            method: "POST",
            dataType : "JSON",
            data : { clientID : c_id,name:vall, operation:"project" },
            success: function(response){
                ths.closest('td').siblings().find("#hiddenField2").val(response.data[0].id);
                ths.closest('td').siblings().find("#projectDescription").val(response.data[0].project_description);
                response.data[0].rate_per_hour = response.data[0].rate_per_hour=="-" ? 0 : response.data[0].rate_per_hour
                ths.closest('td').siblings().find("#rate").val(response.data[0].rate_per_hour);
                RealPrice.push(response.data[0].rate_per_hour);
                ths.closest('td').siblings().find("#project_is_hourly_based").attr('readonly', true);
                if(response.data[0].is_hourly_based == 0){
                    ths.closest('td').siblings().find("#project_is_hourly_based").val("No");
                    ths.closest('td').siblings().find("#project_hours").attr('readonly', true);
                    ths.closest('td').siblings().find("#project_hours").val("N/A");
                }else{
                    ths.closest('td').siblings().find("#project_is_hourly_based").val("Yes");
                }
            }
        })
    })

    // Calculate total amount with taxes added
    $(document).on("change","#project_hours,#rate,#clientPercentage",function(){
        var amount = 0
        var vall = $(this).val();
        var a = $(this).closest('tr').index();
        if(RealPrice[a-1] == 0 && $(this)[0].id == "rate"){
            var b = $(this).val()
            $(this).parent().parent().parent().siblings().find('#update_price').val(b +" "+client_currency);
        }else{
            console.log(RealPrice);
            $(this).parent().parent().parent().siblings().find('#update_price').val(RealPrice[a-1]*vall +" "+ client_currency);
        }
        var amountAfterTexAdded=0
        for(var i = 0;i<RealPrice.length;i++){
            amount += parseInt($(".update_price").eq(i).val());
        }
        var price = 0;
        for(var j = 0;j< $(".clientPercentage").length;j++){
            var perc = parseInt($(".clientPercentage").eq(j).val());
            price += amount*(perc/100);
            $(".clientPercentage").eq(j).closest("td").next().find("input").val(amount*(perc/100) +" "+ client_currency);
        }
        amountAfterTexAdded = amount+price;
        $(".totalPrice").val(amountAfterTexAdded +" "+ client_currency)
    })
    
    // Open form to add clients
    $(document).on("click",".addClient",function(){
        $(".main-content").toggleClass("inactive");
        $(".popup_registration").addClass("popup_registration_active").removeClass("inactive");
    })

    // Add another taxes
    $(".addTax").click(function(){
        $('.clone').append(tax);
    })

    // Deleting the invoice
    $(document).on("click",".deleteInvoice",function(){
        var id = $(this).attr('data-attr')
        setTimeout(() => {
          bootbox.confirm({
            message: "Are you sure you want to delete this Invoice",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
              if(result==true){
                $.ajax({
                url:'./getInvoiceData.php',
                dataType: "JSON",
                method: "POST",
                data: { id:id,operation:"delete"},
                success:function(res){
                    window.location.href = "http://localhost:8000/invoice_task/get_invoices.php";
                }
                });
              }
            }
        });
        }, 100);
      });

    //   Deleting the clients
    $(document).on("click",".deleteclient",function(){
        var msg = "This client has ";
        var email = $(this).closest('tr').find('td').eq(2).text();
        var id = $(this).closest('tr').index()
        $.ajax({
          url:'./getDetailOfClient.php',
          dataType: "json",
          method: "POST",
          data: { email: email, id:clientIds[id], operation:"delete" },
          success:function(res){
            msg+= res.data.length + " projects and names of project is/are "
            for( i = 0;i<res.data.length;i++){
              msg += '"'+res.data[i].project_name+'", '
            }
            msg+= " and has "+res.data2.length+" invoices. Are you sure you want to delete client."
          }
        })
        setTimeout(() => {
          bootbox.confirm({
            message: msg,
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
              if(result==true){
                $.ajax({
                url:'./delete_client.php',
                dataType: "JSON",
                method: "POST",
                data: {email: email, id:clientIds[id]},
                success:function(res){
                    window.location.href = "http://localhost:8000/invoice_task/clients.php";
                }
                });
              }
            }
        });
        }, 100);
      });

// Deleting the projects
      $(document).on("click",".deleteProject",function(){
        var msg = "This project has ";
          var projectName = $(this).closest('tr').find('td').eq(2).text();
          var id = $(this).attr('data-attr')
          $.ajax({
            url:'./getDetailOfClient.php',
            dataType: "json",
            method: "POST",
            data: { name: projectName, id:id, operation:"deleteProject" },
            success:function(res){
              msg += res.data.length + " Invoice connected with it."
            }
          })
          setTimeout(() => {
            bootbox.confirm({
              message: msg,
              buttons: {
                  confirm: {
                      label: 'Yes',
                      className: 'btn-success'
                  },
                  cancel: {
                      label: 'No',
                      className: 'btn-danger'
                  }
              },
              callback: function (result) {
                if(result==true){
                   $.ajax({
                    url:'./delete_project.php',
                    dataType: "json",
                    method: "POST",
                    data: { id:id, operation:"deleteProject" },
                    success:function(res){
                      window.location.href = "http://localhost:8000/invoice_task/projects.php";
                    }
                });
                }
              }
          });
          }, 100);
         
      })

      $(document).on("click",".changep",function(){
          var str = $(".hiddendiv").html();
          str = str.replace('for=""','for="new_pass"');
          str = str.replace('name=""','name="new_pass"');
          str = str.replace('id=""','id="new_pass"');
        //   console.log(s);
        $(".pass").append(str);
        $("label[for='new_pass']").text("New Password");
        var str = $(".hiddendiv").html();
          str = str.replace('for=""','for="conf_pass"');
          str = str.replace('name=""','name="conf_pass"');
          str = str.replace('id=""','id="conf_pass"');
        //   console.log(s);
        $(".pass").append(str);
        $("label[for='conf_pass']").text("Confirm Password");
        $(this).prop("disabled",true)
      })
// Restoring projects, clients and invoices
      $(document).on("click",".restoreProject,.restoreClient,.restoreInvoice",function(){
          var id = $(this).attr('data-attr')
          var operation,msg;
          if($(this).hasClass("restoreProject")){
              msg = "Are you sure you want to restore this Project"
              operation = "project"
          }else if($(this).hasClass("restoreClient")){
            msg = "Are you sure you want to restore this Client"
            operation = "client"
          }else{
            msg = "Are you sure you want to restore this Invoice"
            operation = "invoice"
          }
          setTimeout(() => {
            bootbox.confirm({
              message: msg,
              buttons: {
                  confirm: {
                      label: 'Yes',
                      className: 'btn-success'
                  },
                  cancel: {
                      label: 'No',
                      className: 'btn-danger'
                  }
              },
              callback: function (result) {
                if(result==true){
                   $.ajax({
                    url:'./restore.php',
                    dataType: "json",
                    method: "POST",
                    data: { id:id, operation:operation },
                    success:function(res){
                        if(res.operation == "project"){
                            if(res.message == "Can't restore project, Client is deleted."){
                                bootbox.alert(res.message);
                            }else{
                                window.location.href = "http://localhost:8000/invoice_task/projects.php";
                            }
                        }else if(res.operation == "client"){
                            window.location.href = "http://localhost:8000/invoice_task/clients.php";
                        }else{
                            window.location.href = "http://localhost:8000/invoice_task/get_invoices.php";
                        }
                    }
                });
                }
              }
          });
          }, 100);
        });
     
        // Change amount as per hours added
    $(document).on("change","#is_hourly_based",function(){
        var is_hour = $(this).val();
        if(is_hour==1){
            var str = $(".clone_hidden_rate").html();
            str = str.replace(/rate_per/g,"rate_per_hour")
            $(".rate").append(str);
        }else{
            $(".rate>.input-group").remove();
        }
    })

    // Remove tax
    $(document).on("click",".removeTax",function(){
        $(this).closest(".edit_tax").remove();
    })

    // Select Currency type in placeholder
    var currency;
    $(document).on("change","#selected_client_id",function(){
        var val = $(this).val();
        currency = "";
        $.ajax({
            url:'./getDetailOfClient.php',
            dataType: "json",
            method: "POST",
            data: { id:val, operation:"get_currency" },
            success:function(res){
                currency = res.data.currency;
                $("#rate_per,#rate_per_hour").attr("placeholder",res.data.currency);
            }
        })
    })

    // $(document).on("change","#rate_per_hour",function(){
    //     var val = $(this).val();
    //     if($(this).closest("div").hasClass("update")){
    //         console.log($(this).closest("div").hasClass("update"));
    //         $(this).val(val+" "+currency);
    //     }else{
    //         $(this).val(val);
    //     }
    // })

    // Get active link
    var url = window.location.href;
    $('a[href="'+url+'"]').addClass("activelink");
    

    $('form[name="project-form"]').validate({
        rules: {
        selected_client_id: 'required',
        project_name: 'required',
        description: 'required',
        is_hourly_based: 'required',
        },
        messages: {
        selected_client_id: 'Please select client',
        project_name: 'Please enter Project Name',
        description: 'Please enter Project Description',
        is_hourly_based: 'Please select Project type'
        },
        errorElement: 'em',
        errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
    
        if (element.prop('type') === 'checkbox') {
            error.insertAfter( element.next('label'));
        } else {
            error.insertAfter(element);
        }
        },
        highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid').removeClass('is-valid');
        },
        unhighlight: function (element, errorClass, validClass) {
        $(element).addClass('is-valid').removeClass('is-invalid');
        }
    });


    $('form[name="client_form"]').validate({
        rules: {
        name: 'required',
        email: {
            required: true,
            email: true
        },
        phone: {
            required: true,
            minlength: 10
        },
        gstin: {
            required: true,
            minlength: 15
        },
        currency: 'required',
        address: 'required',
        place_to_supply: 'required',
        state_code: {
            required: true,
            minlength: 6
          },
        },
        messages: {
        name: 'Please enter Client Name',
        email: 'Please enter a valid Email Address',
        phone: {
            required: 'Please enter a Phone No.',
            minlength: 'Your Phone no. must consist of at least 10 characters'
        },
        gstin: {
        required: 'Please enter a GST number',
        minlength: 'Your GST no. must consist of at least 15 characters'
        },
        currency: 'Please select a currency',
        address: 'Please enter client address',
        place_to_supply: 'Please enter Place to supply',
        state_code: {
            required: 'Please enter a state code number',
            minlength: 'Your state code must consist of at least 6 characters'
        },
        },
        errorElement: 'em',
        errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
    
        if (element.prop('type') === 'checkbox') {
            error.insertAfter( element.next('label'));
        } else {
            error.insertAfter(element);
        }
        },
        highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid').removeClass('is-valid');
        },
        unhighlight: function (element, errorClass, validClass) {
        $(element).addClass('is-valid').removeClass('is-invalid');
        }
    });

})