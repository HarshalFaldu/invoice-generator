<?php

  session_start();
  // echo $_SESSION['email'];
  if(isset($_SESSION['email'])){
    header("location:dashboard.php");
  }
  require("connection.php");
  require('vendor/autoload.php');
  use Rakit\Validation\Validator;

  $validator = new Validator;

  $validation = $validator->make($_POST, [
    'email'                 => 'required|email',
    'password'              => 'required',
  ]);
  $error = "";
  $validation->validate();
  if($_POST){
    if ($validation->fails()) {
        $errors = $validation->errors();
        $errors_array = $errors->firstOfAll();
        $_SESSION['email_error'] = $errors_array['email'];
        $_SESSION['password_error'] = $errors_array['password'];
        header("location:login.php");
        exit;
      } else {
        $sql = "Select email,password from users";
        $result = $conn->query($sql);
        $pass = sha1($_POST['password']);
        if($result->num_rows > 0 ){
            while($row = $result->fetch_array()){
                if($_POST['email'] === $row['email'] && $pass === $row['password']){
                  //Header to another page
                  $_SESSION['email'] = $row['email'];
                  header("location:./dashboard.php");
                }else{
                  $_SESSION['error'] = "Login credentials fail";
                }
            }
        }
    }
  }

    
    
    

  ?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>Invoice System</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="./css/argon.css?v=1.2.0" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="./js/main.js"></script>
</head>

<body class="bg-default">
  
  <nav id="navbar-main" class="navbar navbar-horizontal navbar-transparent navbar-main navbar-expand-lg navbar-light">
  </nav>
  
  <div class="main-content">
    
    <div class="header bg-gradient-primary py-lg-6 pt-lg-4">
      <div class="container">
        <div class="header-body text-center mb-7">
          <div class="row justify-content-center">
            <div class="col-xl-5 col-lg-6 col-md-8 px-5">
              <h1 class="text-white">Welcome!</h1>
              <p class="text-lead text-white">Welcome to the Invoice Generating System.</p>
            </div>
          </div>
        </div>
      </div>
      <div class="separator separator-bottom separator-skew zindex-100">
        <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
          <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
        </svg>
      </div>
    </div>
    
    <div class="container mt--8 pb-5">
      <div class="row justify-content-center">
        <div class="col-lg-5 col-md-7">
          <div class="card bg-secondary border-0 mb-0">
            <div class="card-body px-lg-5 py-lg-5">
            
              <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST" id = "Login_form">
                <div class="form-group mb-3">
                  <div class="input-group input-group-merge input-group-alternative">
                    <div class="input-group-prepend">
                    <!-- <span style="color: red;">*</span> -->
                      <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                    </div>
                    <input class="form-control" placeholder="Email" type="text" name="email" id="email">
                  </div>
                  <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['email_error'])){echo$_SESSION['email_error'];} ?></span>
                </div>
                <div class="form-group">
                  <div class="input-group input-group-merge input-group-alternative">
                    <div class="input-group-prepend">
                    <!-- <span style="color: red;">*</span> -->
                      <span class="input-group-text"><i class="fa fa-key"></i></span>
                    </div>
                    <input class="form-control" placeholder="Password" type="password" name="password" id="password">
                  </div>
                  <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['password_error'])){echo$_SESSION['password_error'];}if(isset($_SESSION['error'])){echo$_SESSION['error'];} ?></span>
                </div>
                <!-- <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['error'])){echo$_SESSION['error'];} ?></span> -->
                <div class="text-center">
                  <input type="submit" class="btn btn-primary my-4 Login" value="Log In">
                </div>
              </form>
            </div>
          </div>
          <div class="row mt-3">
            <div class="col-6">
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  


</body>
<?php

unset($_SESSION['email_error']);
unset($_SESSION['password_error']);
unset($_SESSION['error']);

?>



</html>