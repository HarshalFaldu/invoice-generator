<?php

session_start();
if(!isset($_SESSION['email'])){
  header("location:dashboard.php");
}
$error = "";

require('./connection.php');

require('vendor/autoload.php');

use Rakit\Validation\Validator;
$validator = new Validator;

$validation = $validator->make($_POST + $_FILES, [
    'project_name'          => 'required',
    'description'           => 'required',
    'is_hourly_based'       => 'required',
]);
$validation->validate();
if($_POST){
    if ($validation->fails()) {
        $errors = $validation->errors();
        $errors_array = $errors->firstOfAll();
        $_SESSION['Pname_error'] = $errors_array['project_name'];
        $_SESSION['desc_error'] = $errors_array['description'];
        $_SESSION['hourly_error'] = $errors_array['is_hourly_based'];
        $_SESSION['rate_error'] = $errors_array['rate_per_hour'];

        if($_SESSION['Pname_error'] === "The Project name is required" || $_SESSION['desc_error'] === "The Description is required" || $_SESSION['hourly_error'] === "The Is hourly based is required" || $_SESSION['rate_error'] === "The Rate per hour is required"){
          $_SESSION['error_in_adding'] = "Fields can't be empty";
        }else{
          $_SESSION['error_in_adding'] = "Error while Inseting data";
        }
        header('location:./projects.php');
        exit;
    }else{
    if(!isset($_POST['rate_per_hour'])){
      $number = '-';  
    }else{
      $number = str_replace(['+', '-'], '', filter_var($_POST['rate_per_hour'], FILTER_SANITIZE_NUMBER_INT));
      number_format($number);
    }
    $sql = 'INSERT INTO projects (client_id,project_name,project_description,is_hourly_based,rate_per_hour,is_deleted) Values ('. $_POST['selected_client_id'] .',"'. $_POST['project_name'] .'","'. $_POST['description'].'","'.$_POST['is_hourly_based'].'","'.$number.'","active");';
    if (!$conn->query($sql) === TRUE) {
        $error = $conn->error;
        $_SESSION['error_in_adding'] = $error;
    } else{
      $_SESSION['add_success'] = "Project added successfully";
    }
    $conn->close();

  }

}

?>


<!DOCTYPE html>
<html>

<?php include('./head_files.php') ?>

<body class="body">
  <!-- Sidenav -->
<?php  include('./sidebar.php');
include('./header.php');
?>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    <!-- Header -->
    <!-- Header -->
    <div class="header  pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-baseline py-5">
            <div class="col-lg-7 col-7">
              <h6 class="h2  d-inline-block mb-0">All Projects</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">All Projects</li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-5 col-5 text-right ">
              <button class="btn btn-neutral addClient" type="button"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp; Add New Projects</button>
              <a class="btn btn-danger " type="button" href="./removed_projects.php"><i class="fa fa-trash-o"></i>&nbsp;&nbsp; Removed Projects</a>
            </div>
            <!-- <div class="col-lg-3 col-5 text-right" style="max-width:20%;">
            </div> -->
            <?php 
            if(isset($_SESSION['error_in_adding'])){
              ?>
              <span style="margin-left:14px;width:400px;color:white;" class="alert alert-danger fa fa-times"><?php if(isset($_SESSION['error_in_adding'])){ echo "    ".$_SESSION['error_in_adding']; }?></span>
              <?php
              }
               
              if(isset($_SESSION['add_success'])){
              ?>
              <span style="margin-left:14px;width:400px;color:white;" class="alert alert-success fa fa-check"><?php if(isset($_SESSION['add_success'])){ echo "    ".$_SESSION['add_success']; }?></span>
              <?php
              }
            ?>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6 table-responsive">
      
    <table id="projects" class="table align-items-center table-flush">
    <thead>
      <tr>
          <th>Id</th>
          <th>Client Name</th>
          <th>Project Name</th>
          <th>Project Description</th>
          <th>Is Hourly based</th>
          <th>Rates per hour</th>
          <th>Edit</th>
          <th>Delete</th>
      </tr>
    </thead>
  </table>
    </div>
  </div>

   <!-- Add Clients code -->

  <div class="popup_registration inactive">
            <div class="main-content_2">
    
            <div class="container-fluid">
            
            <div class="container pt-7">
            <div class="row justify-content-center">
                <div class="col-lg-7 col-md-7">
                <div class="card bg-secondary border-0 mb-0">
                    <div class="card-body px-lg-5 py-lg-5">
                    
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST" name="project-form" role="form">
                        <div class="form-group mb-3">
                        <div class="input-group input-group-merge">
                        <label for="name" class="input-group-text" style="width:155px;color:black;">Select Client: </label>
                            <!-- <input class="form-control" placeholder="Name" type="text" name="name" id="name"> -->
                            <select  class="form-control" name="selected_client_id" id="selected_client_id" style="width:200px;">
                            <option value="null" selected >Select Client ID</option>
                            <?php
                            require('./connection.php');
                            $select_client = 'select id,CONCAT(name,"(",email,")") as client from clients where is_deleted="active";';
                            $run = $conn->query($select_client);
                            if ($run->num_rows > 0) {
                                while($row = $run->fetch_assoc()) { ?>
                                    <option value="<?php echo $row['id'];?>"><?php echo $row['client'];?></option>
                                    <?php
                                }
                            }

                            ?>
                            </select>
                        </div>
                        </div>


                        <div class="form-group">
                        <div class="input-group input-group-merge">
                        <label for="name" class="input-group-text" style="width:155px;color:black;">Project Name: </label>
                            <input class="form-control" type="text" name="project_name" id="project_name">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['Pname_error'])){echo$_SESSION['Pname_error'];} ?></span>

                        </div>

                        <div class="form-group">
                        <div class="input-group input-group-merge">
                        <label for="name" class="input-group-text" style="width:155px;color:black;">Project Description:</label>
                            <input class="form-control" placeholder="Project Description(255 words max)" type="text" name="description" id="description">
                        </div>
                        
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['desc_error'])){echo$_SESSION['desc_error'];} ?></span>

                        </div>
                        
                        <div class="form-group">
                        <div class="input-group input-group-merge">
                        <label for="name" class="input-group-text" style="width:155px;color:black;">Is Hourly Based: </label>
                            <select class="form-control" name="is_hourly_based" id="is_hourly_based">
                            <option value="" selected>Is Hourly Paid/Not</option>
                            <option value="0" >No</option>
                            <option value="1">Yes</option>
                            </select>
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['hourly_error'])){echo$_SESSION['hourly_error'];} ?></span>
                        </div>
                        
                        <div class="form-group clone_hidden_rate">
                        <div class="input-group input-group-merge">
                        <label for="name" class="input-group-text" style="width:155px;color:black;">Rate: </label>
                            <input class="form-control" placeholder="Rate" type="text" name="rate_per" id="rate_per">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['rate_error'])){echo$_SESSION['rate_error'];} ?></span>
                        </div>
                        
                        <div class="form-group rate">
                            
                        </div>
                        

                        <!-- Taxes -->
                        

                        <div class="text-center">
                        <input type="submit" class="btn btn-success my-4" placeholder="Sign in">
                        <a type="button" class="btn btn-danger my-4" href="./projects.php">Cancel</a>
                        </div>
                    </form>
                    </div>
                </div>
                <div class="row mt-3">
                    
                </div>
                </div>
            </div>
            </div>
            </div>
        </div>
          </div>
          
<?php include('./footer.php'); ?>
 <script>
 var dt2 = $('#projects').DataTable({
    scrollY:        "270px",
    scrollX:        true,
    scrollCollapse: true,
    fixedColumns: true
  });
    $.ajax({
        url: "./get_projects.php",
        dataType:"JSON",
        method: "POST",
        data: {operation:"project"},
        success : function(jsonObject){
          console.log(jsonObject);
            for(var i = 0;i < jsonObject.data.length; i++){
              jsonObject.data[i].is_hourly_based = jsonObject.data[i].is_hourly_based==1 ? "Yes" : "No";
              jsonObject.data[i].rate_per_hour = jsonObject.data[i].rate_per_hour== "-" ? "N/A" :jsonObject.data[i].rate_per_hour+" "+jsonObject.data[i].currency ;
                var row = [i+1,jsonObject.client_data[i].name,jsonObject.data[i].project_name,jsonObject.data[i].project_description,jsonObject.data[i].is_hourly_based,jsonObject.data[i].rate_per_hour]
                var edit = `<a class="edit btn btn-info" data-attr="${jsonObject.data[i].id}" href="./edit_project.php?id=${jsonObject.encode[i]}"><i class="fa fa-pencil"></i></a>`
                dt2.row.add([row[0],row[1],row[2],row[3],row[4],row[5],edit,`<a class="deleteProject btn btn-danger" data-attr="${jsonObject.data[i].id}"><i class="fa fa-trash"></i></a>`]).draw();
            }
        }
    });

    
 </script>
  <?php
    if(isset($_SESSION['error_in_adding'])){
      unset($_SESSION['error_in_adding']);
    }
    if(isset($_SESSION['add_success'])){
      unset($_SESSION['add_success']);
    }
    if(isset($_SESSION['Pname_error'])){
      unset($_SESSION['Pname_error']);
    }
    if(isset($_SESSION['desc_error'])){
      unset($_SESSION['desc_error']);
    }
    if(isset($_SESSION['hourly_error'])){
      unset($_SESSION['hourly_error']);
    }
    if(isset($_SESSION['rate_error'])){
      unset($_SESSION['rate_error']);
    }
    
 ?>
</body>

</html>
