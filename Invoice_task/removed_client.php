<?php

session_start();
if(!isset($_SESSION['email'])){
  header("location:dashboard.php");
}

$error = "";
require('./connection.php');

?>


<!DOCTYPE html>
<html>

<?php include('./head_files.php') ?>

<body class="body">
  <!-- Sidenav -->
<?php 
include('./sidebar.php');
include('./header.php');?>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    
    <!-- Header -->
    <!-- Header -->
    <div class="header  pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2  d-inline-block mb-0">Removed Clients</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Removed Clients</li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-6 col-5 text-right addClient">
              
            </div>
            <?php 
            if(isset($_SESSION['error_in_adding'])){
            ?>
            <span style="margin-left:14px;width:400px;color:white;" class="alert alert-danger fa fa-times"><?php if(isset($_SESSION['error_in_adding'])){ echo "    ".$_SESSION['error_in_adding']; }?></span>
            <?php
            }
             
            if(isset($_SESSION['add_success'])){
            ?>
            <span style="margin-left:14px;width:400px;color:white;" class="alert alert-success fa fa-check"><?php if(isset($_SESSION['add_success'])){ echo "    ".$_SESSION['add_success']; }?></span>
            <?php
            }
            ?>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6 table-responsive">
      
    <table id="my-example1" class="table align-items-center table-flush">
    <thead>
      <tr>
          <th>Id</th>
          <th>Name</th>
          <th>Email</th>
          <th>Phone</th>
          <th>GSTIN</th>
          <th>Currency</th>
          <th>Address</th>
          <th>Place To Supply</th>
          <th>State Code</th>
          <th>Tax included</th>
          <th>Restore</th>
      </tr>
    </thead>
  </table>
    </div>
  </div>

  <!-- Add Clients code -->
  
  <?php include('./footer.php'); ?>
  <script>
  var clientIds = [];
  var dt = $('#my-example1').DataTable({
    scrollY:        "300px",
    scrollX:        true,
    scrollCollapse: true,
    columnDefs: [
            { width: 100 }
        ],
    fixedColumns: true
  });
    $.ajax({
        url: "./get_data.php",
        dataType: "JSON",
        method: "POST",
        data: {operation:"get_removed_client"},
        success : function(jsonObject){
            // var jsonObject = JSON.parse(response);
            console.log(jsonObject);
            for(var i = 0;i < jsonObject.data.length; i++){
                var row = [i+1,jsonObject.data[i].name,jsonObject.data[i].email,jsonObject.data[i].phone,jsonObject.data[i].gstin,jsonObject.data[i].currency,jsonObject.data[i].address,jsonObject.data[i].place_of_supply,jsonObject.data[i].state_code,jsonObject.data[i].tax_included]
                clientIds.push(jsonObject.data[i].id)
                var edit_client = `<a class="restoreClient btn btn-info" data-attr="${jsonObject.data[i].id}" >Restore</a>`;
                dt.row.add([row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8],row[9],edit_client]).draw();
            }
        }
    });
    
    
    new Pidie();
  </script>


 <?php
    unset($_SESSION['error_in_adding']);
    unset($_SESSION['add_success']);
 ?>

</body>

</html>
