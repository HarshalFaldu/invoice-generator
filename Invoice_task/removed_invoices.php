<?php

session_start();
if(!isset($_SESSION['email'])){
  header("location:dashboard.php");
}

$error = "";
require('./connection.php');

?>


<!DOCTYPE html>
<html>

<?php include('./head_files.php') ?>

<body class="body">
  <!-- Sidenav -->
<?php 
include('./sidebar.php');
include('./header.php');?>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    
    <!-- Header -->
    <!-- Header -->
    <div class="header  pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2  d-inline-block mb-0">Removed Invoices</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Removed Invoices</li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-6 col-5 text-right addClient">
              
            </div>
            <?php 
            if(isset($_SESSION['error_in_adding'])){
            ?>
            <span style="margin-left:14px;width:400px;color:white;" class="alert alert-danger fa fa-times"><?php if(isset($_SESSION['error_in_adding'])){ echo "    ".$_SESSION['error_in_adding']; }?></span>
            <?php
            }
             
            if(isset($_SESSION['add_success'])){
            ?>
            <span style="margin-left:14px;width:400px;color:white;" class="alert alert-success fa fa-check"><?php if(isset($_SESSION['add_success'])){ echo "    ".$_SESSION['add_success']; }?></span>
            <?php
            }
            ?>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6 table-responsive">
      
    <table id="my-invoice" class="table align-items-center table-flush">
    <thead>
      <tr>
          <th>Id</th>
          <th>Created Date</th>
          <th>Due Date</th>
          <th>Client Email</th>
          <th>Projects</th>
          <th>Amount</th>
          <th>Status</th>
          <th>Delete</th>
      </tr>
    </thead>
  </table>
    </div>
  </div>

  <!-- Add Clients code -->
  
  <?php include('./footer.php'); ?>
  <script>
  var dt = $('#my-invoice').DataTable();
    $.ajax({
        url: "./getInvoiceData.php",
        dataType: "JSON",
        method: "POST",
        data: {operation:"removed_invoice"},
        success : function(jsonObject){
            for(var i = 0;i < jsonObject.data.length; i++){
                if(jsonObject.data[i].status==0){
                    jsonObject.data[i].status = "<span style='color:red'>Not Paid</span>"
                }else{
                    jsonObject.data[i].status = "<span style='color:green'>Paid</span>"
                }
                var row = [i+1,jsonObject.data[i].invoice_date,jsonObject.data[i].due_date,jsonObject.client_name[i].email,jsonObject.data[i].project_id,jsonObject.data[i].amount+" "+jsonObject.data[i].currency,jsonObject.data[i].status]
                var edit_client = `<a class="restoreInvoice btn btn-info" data-attr="${jsonObject.data[i].id}" >Restore</a>`;
                dt.row.add([row[0],row[1],row[2],row[3],row[4],row[5],row[6],edit_client]).draw();
            }
        }
    });
    
    
    new Pidie();
  </script>


 <?php
    unset($_SESSION['error_in_adding']);
    unset($_SESSION['add_success']);
 ?>

</body>

</html>
