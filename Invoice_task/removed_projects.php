<?php

session_start();
if(!isset($_SESSION['email'])){
  header("location:dashboard.php");
}

$error = "";
require('./connection.php');
?>


<!DOCTYPE html>
<html>

<?php include('./head_files.php') ?>

<body class="body" style="margin-left:20px;">
  <!-- Sidenav -->
<?php 
include('./sidebar.php');
include('./header.php');?>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    
    <!-- Header -->
    <!-- Header -->
    <div class="header  pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2  d-inline-block mb-0">Removed Projects</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Removed Projects</li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-6 col-5 text-right addClient">
              
            </div>
            <?php 
            if(isset($_SESSION['error_in_adding'])){
            ?>
            <span style="margin-left:14px;width:400px;color:white;" class="alert alert-danger fa fa-times"><?php if(isset($_SESSION['error_in_adding'])){ echo "    ".$_SESSION['error_in_adding']; }?></span>
            <?php
            }
             
            if(isset($_SESSION['add_success'])){
            ?>
            <span style="margin-left:14px;width:400px;color:white;" class="alert alert-success fa fa-check"><?php if(isset($_SESSION['add_success'])){ echo "    ".$_SESSION['add_success']; }?></span>
            <?php
            }
            ?>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6 table-responsive">
      
    <table id="projects1" class="table align-items-center table-flush">
    <thead>
    <tr>
          <th>Id</th>
          <th>Client Name</th>
          <th>Project Name</th>
          <th>Project Description</th>
          <th>Is Hourly based</th>
          <th>Rates per hour</th>
          <th>Restore</th>
      </tr>
    </thead>
  </table>
    </div>
  </div>

  <!-- Add Clients code -->
  
  <?php include('./footer.php'); ?>
  <script>
  var clientIds = [];
  var dt2 = $('#projects1').DataTable();
    $.ajax({
        url: "./get_projects.php",
        dataType:"JSON",
        method: "POST",
        data: {operation:"removed_project"},
        success : function(jsonObject){
          for(var i = 0;i < jsonObject.data.length; i++){
            jsonObject.data[i].is_hourly_based = jsonObject.data[i].is_hourly_based==1 ? "Yes" : "No";
              jsonObject.data[i].rate_per_hour = jsonObject.data[i].rate_per_hour== "-" ? "N/A" :jsonObject.data[i].rate_per_hour+" "+jsonObject.data[i].currency ;
                var row = [i+1,jsonObject.client_data[i].name,jsonObject.data[i].project_name,jsonObject.data[i].project_description,jsonObject.data[i].is_hourly_based,jsonObject.data[i].rate_per_hour]
                var edit = `<a class="restoreProject btn btn-info" data-attr="${jsonObject.data[i].id}" >Restore</a>`
                dt2.row.add([row[0],row[1],row[2],row[3],row[4],row[5],edit]).draw();
            }
        }
    });
    
    
    new Pidie();
  </script>


 <?php
    unset($_SESSION['error_in_adding']);
    unset($_SESSION['add_success']);
    unset($_SESSION['name_error']);
    unset($_SESSION['address_error']);
    unset($_SESSION['place_error']);
    unset($_SESSION['state_error']);
 ?>

</body>

</html>
