<?php

session_start();
if(!isset($_SESSION['email'])){
  header("location:dashboard.php");
}

$error = "";
require('./connection.php');

require('vendor/autoload.php');

$sql2 = 'SELECT * FROM company_details';
$result2 = $conn->query($sql2);

$row = [];
while($row2 = $result2->fetch_array(MYSQLI_ASSOC)){
    $row[] = $row2;
}

?>


<!DOCTYPE html>
<html>

<?php include('./head_files.php') ?>

<body class="body">
  <!-- Sidenav -->
<?php 
include('./sidebar.php');
include('./header.php');?>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    
    <!-- Header -->
    <!-- Header -->
    <div class="header pb-3">
      <div class="container-fluid">
            <?php 
            if(isset($_SESSION['error_in_adding'])){
            ?>
            <span style="margin-left:14px;width:400px;color:white;" class="alert alert-danger fa fa-times"><?php if(isset($_SESSION['error_in_adding'])){ echo "    ".$_SESSION['error_in_adding']; }?></span>
            <?php
            }
             
            if(isset($_SESSION['add_success'])){
            ?>
            <span style="margin-left:14px;width:400px;color:white;" class="alert alert-success fa fa-check"><?php if(isset($_SESSION['add_success'])){ echo "    ".$_SESSION['add_success']; }?></span>
            <?php
            }
            ?>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
    </div>
  </div>

   <!-- Add Clients code -->

  <div class="popup_registration">
            <div class="main-content_2">
    
            <div class="container-fluid">
            
            <div class="container pt-7">
            <div class="row justify-content-center">
                <div class="col-lg-11 col-md-11">
                <div class="card bg-secondary border-0 mb-0">
                <div class="card-body px-lg-5 py-lg-5">
                    <form action="./update_admin.php" method="POST">

                    <hr class="my-4">
           
           <h6 class="heading text-muted mb-5" style="text-align: center;">Company's Details</h6>

                    <div class="form-group" style="display:flex;justify-content:space-between;margin-bottom:-5px;">
                        <div class="form-group" style="display:inline;">
                        <div class="input-group input-group-merge" >
                            <label for="fname" class="input-group-text" style="width:140px;color:black;">First Name </label>
                            <input class="form-control"  type="text" name="fname" id="fname" style="width:275px;" value="<?php echo $row[0]['first_name']; ?>">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['fname_error'])){echo$_SESSION['fname_error'];} ?></span>
                        </div>

                        <div class="form-group" style="display:inline;">
                        <div class="input-group input-group-merge">
                            <label for="lname" class="input-group-text" style="width:140px;color:black;">Last Name </label>
                            <!-- </div> -->
                            <input class="form-control" type="text" name="lname" id="lname" style="width:275px;" value="<?php echo $row[0]['last_name']; ?>">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['lname_error'])){echo$_SESSION['lname_error'];} ?></span>
                        </div>
                    </div>
                        
                    <div class="form-group" style="display:flex;justify-content:space-between;margin-bottom:-5px;">
                        <div class="form-group" style="display:inline;">
                        <div class="input-group input-group-merge" >
                            <label for="cname" class="input-group-text" style="width:140px;color:black;">Company's Name </label>
                            <input class="form-control"  type="text" name="cname" id="cname" style="width:275px;" value="<?php echo $row[0]['company_full_title']; ?>">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['cname_error'])){echo$_SESSION['cname_error'];} ?></span>
                        </div>

                        <div class="form-group" style="display:inline;">
                        <div class="input-group input-group-merge">
                            <label for="website" class="input-group-text" style="width:140px;color:black;">Website </label>
                            <!-- </div> -->
                            <input class="form-control" type="text" name="website" id="website" style="width:275px;" value="<?php echo $row[0]['website_address']; ?>">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['website_error'])){echo$_SESSION['website_error'];} ?></span>
                        </div>
                    </div> 

                    <div class="form-group" style="display:flex;justify-content:space-between;margin-bottom:-5px;">
                        <div class="form-group" style="display:inline;">
                        <div class="input-group input-group-merge">
                            <label for="cmail" class="input-group-text" style="width:140px;color:black;">Company's Email </label>
                            <!-- </div> -->
                            <input class="form-control" type="email" name="cmail" id="cmail" style="width:275px;" value="<?php echo$row[0]['mail_address']; ?>">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['cmail_error'])){echo$_SESSION['cmail_error'];} ?></span>
                        </div>

                        <div class="form-group" style="display:inline;">
                        <div class="input-group input-group-merge" >
                            <label for="address" class="input-group-text" style="width:140px;color:black;">Address </label>
                            <input class="form-control"  type="text" name="address" id="address" style="width:275px;" value="<?php echo $row[0]['company_address']; ?>">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['address_error'])){echo$_SESSION['address_error'];} ?></span>
                        </div>  
                    </div>  

                    <div class="form-group" style="display:flex;justify-content:space-between;margin-bottom:-5px;">
                        
                        <div class="form-group" style="display:inline;">
                        <div class="input-group input-group-merge">
                            <label for="mobile" class="input-group-text" style="width:140px;color:black;">Mobile No. </label>
                            <!-- </div> -->
                            <input class="form-control" type="number" name="mobile" id="mobile" style="width:275px;" value="<?php echo $row[0]['mobile_number']; ?>">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['mobile_error'])){echo$_SESSION['mobile_error'];} ?></span>
                        </div>

                    </div> 

                    <hr class="my-4">
           
           <h6 class="heading text-muted mb-5" style="text-align: center;">Company's Unique Identities</h6>
                    <div class="form-group" style="display:flex;justify-content:space-between;margin-bottom:-5px;">
                        <div class="form-group" style="display:inline;">
                        <div class="input-group input-group-merge">
                            <label for="CIN" class="input-group-text" style="width:140px;color:black;">CIN Number </label>
                            <!-- </div> -->
                            <input class="form-control" type="text" name="CIN" id="CIN" style="width:275px;" value="<?php echo $row[0]['CIN_number']; ?>">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['CIN_error'])){echo$_SESSION['CIN_error'];} ?></span>
                        </div>

                        <div class="form-group" style="display:inline;">
                        <div class="input-group input-group-merge" >
                            <label for="PAN" class="input-group-text" style="width:140px;color:black;">PAN Number </label>
                            <input class="form-control"  type="text" name="PAN" id="PAN" style="width:275px;" value="<?php echo $row[0]['PAN_number']; ?>">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['PAN_error'])){echo$_SESSION['PAN_error'];} ?></span>
                        </div>  
                    </div>  

                    <div class="form-group" style="display:flex;justify-content:space-between;margin-bottom:-5px;">
                        <div class="form-group" style="display:inline;">
                        <div class="input-group input-group-merge">
                            <label for="GST" class="input-group-text" style="width:140px;color:black;">GST Number </label>
                            <!-- </div> -->
                            <input class="form-control" type="text" name="GST" id="GST" style="width:275px;" value="<?php echo $row[0]['GST_number']; ?>">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['GST_error'])){echo$_SESSION['GST_error'];} ?></span>
                        </div>

                        <div class="form-group" style="display:inline;">
                        <div class="input-group input-group-merge" >
                            <label for="IEC" class="input-group-text" style="width:140px;color:black;">IEC Number </label>
                            <input class="form-control"  type="text" name="IEC" id="IEC" style="width:275px;" value="<?php echo $row[0]['IEC_number']; ?>">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['IEC_error'])){echo$_SESSION['IEC_error'];} ?></span>
                        </div>  
                    </div>
                    
                    <div class="form-group" style="display:flex;justify-content:space-between;margin-bottom:-5px;">
                        <div class="form-group" style="display:inline;">
                        <div class="input-group input-group-merge">
                            <label for="HSN" class="input-group-text" style="width:140px;color:black;">HSN Number </label>
                            <!-- </div> -->
                            <input class="form-control" type="text" name="HSN" id="HSN" style="width:275px;" value="<?php echo $row[0]['HSN']; ?>">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['HSN_error'])){echo$_SESSION['HSN_error'];} ?></span>
                        </div> 
                    </div>

                    <hr class="my-4">
           
           <h6 class="heading text-muted mb-5" style="text-align: center;">Company's Bank Details</h6>
                    
                    <div class="form-group" style="display:flex;justify-content:space-between;margin-bottom:-5px;">
                        <div class="form-group" style="display:inline;">
                        <div class="input-group input-group-merge">
                            <label for="bank_name" class="input-group-text" style="width:140px;color:black;">Bank Name </label>
                            <!-- </div> -->
                            <input class="form-control" type="text" name="bank_name" id="bank_name" style="width:275px;" value="<?php echo $row[0]['bank_name']; ?>">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['bank_error'])){echo$_SESSION['bank_error'];} ?></span>
                        </div>

                        <div class="form-group" style="display:inline;">
                        <div class="input-group input-group-merge" >
                            <label for="INR" class="input-group-text" style="width:140px;color:black;">INR Acc. No. </label>
                            <input class="form-control"  type="number" name="INR" id="INR" style="width:275px;" value="<?php echo $row[0]['INR_account_number']; ?>">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['INR_error'])){echo$_SESSION['INR_error'];} ?></span>
                        </div>  
                    </div> 

                    <div class="form-group" style="display:flex;justify-content:space-between;margin-bottom:-5px;">
                        <div class="form-group" style="display:inline;">
                        <div class="input-group input-group-merge">
                            <label for="EURO" class="input-group-text" style="width:140px;color:black;">EURO Acc. No. </label>
                            <!-- </div> -->
                            <input class="form-control" type="number" name="EURO" id="EURO" style="width:275px;" value="<?php echo $row[0]['EURO_account_number']; ?>">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['EURO_error'])){echo$_SESSION['EURO_error'];} ?></span>
                        </div>

                        <div class="form-group" style="display:inline;">
                        <div class="input-group input-group-merge" >
                            <label for="routing" class="input-group-text" style="width:140px;color:black;">Routing No. </label>
                            <input class="form-control"  type="number" name="routing" id="routing" style="width:275px;" value="<?php echo $row[0]['routing_number']; ?>">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['routing_error'])){echo$_SESSION['routing_error'];} ?></span>
                        </div>  
                    </div> 

                    <div class="form-group" style="display:flex;justify-content:space-between;margin-bottom:-5px;">
                        <div class="form-group" style="display:inline;">
                        <div class="input-group input-group-merge" >
                            <label for="swift_code" class="input-group-text" style="width:140px;color:black;">Swift Code </label>
                            <input class="form-control"  type="text" name="swift_code" id="swift_code" style="width:275px;" value="<?php echo $row[0]['swift_code']; ?>">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['swift_error'])){echo$_SESSION['swift_error'];} ?></span>
                        </div> 

                    </div> 

                    <hr class="my-4">
           
           <h6 class="heading text-muted mb-5" style="text-align: center;">Add Notes</h6>
                    <div class="form-group" style="display:flex;justify-content:space-between;margin-bottom:-5px;">
                        <div class="form-group" style="display:inline;">
                        <div class="input-group input-group-merge">
                            <label for="note1" class="input-group-text" style="width:140px;color:black;">Note 1 </label>
                            <!-- </div> -->
                            <input class="form-control" type="text" name="note1" id="note1" style="width:275px;" value="<?php echo $row[0]['note1']; ?>">
                        </div>
                        </div>

                        <div class="form-group" style="display:inline;">
                        <div class="input-group input-group-merge" >
                            <label for="note2" class="input-group-text" style="width:140px;color:black;">Note 2 </label>
                            <input class="form-control"  type="text" name="note2" id="note2" style="width:275px;" value="<?php echo $row[0]['note2']; ?>">
                        </div>
                        </div>  
                    </div> 

                    <div class="form-group" style="display:flex;justify-content:space-between;margin-bottom:-5px;">
                        <div class="form-group" style="display:inline;">
                        <div class="input-group input-group-merge">
                            <label for="note3" class="input-group-text" style="width:140px;color:black;">Note 3 </label>
                            <input class="form-control" type="text" name="note3" id="note3" style="width:275px;" value="<?php echo $row[0]['note3']; ?>">
                        </div>
                        </div>

                        <div class="form-group" style="display:inline;">
                        <div class="input-group input-group-merge" >
                            <label for="note4" class="input-group-text" style="width:140px;color:black;">Note 5 </label>
                            <input class="form-control"  type="text" name="note4" id="note4" style="width:275px;" value="<?php echo $row[0]['note4']; ?>">
                        </div>
                        </div>  
                    </div> 

                    <div class="form-group" style="display:flex;justify-content:space-between;margin-bottom:-5px;">
                        <div class="form-group" style="display:inline;">
                        <div class="input-group input-group-merge">
                            <label for="note5" class="input-group-text" style="width:140px;color:black;">Note 5 </label>
                            <input class="form-control" type="text" name="note5" id="note5" style="width:275px;" value="<?php echo $row[0]['note5']; ?>">
                        </div>
                        </div>

                    </div> 
                    <hr class="my-4">
           
           <h6 class="heading text-muted mb-5" style="text-align: center;">Thank You Message</h6>
                       <div class="form-group" style="display:inline;">
                        <div class="input-group input-group-merge" >
                            <label for="thank_you_message" class="input-group-text" style="width:140px;color:black;">Thank you Note </label>
                            <input class="form-control"  type="text" name="thank_you_message" id="thank_you_message" style="width:275px;" value="<?php echo $row[0]['thank_you_message']; ?>">
                        </div>
                        <span style="color:red;font-size:12px;"><?php if(isset($_SESSION['thank_error'])){echo$_SESSION['thank_error'];} ?></span>
                        </div>  
                    <!-- </div>  -->
                        
                    <div class="text-center" style="margin-top:15px;">
                    <input type="submit" class="btn btn-success my-2" placeholder="Sign in">
                    <a type="button" class="btn btn-danger my-2" href="./dashboard.php">Cancel</a>
                    </div>
                </form>
                    </div>
                </div>
                <div class="row mt-3">
            
                </div>
                </div>
            </div>
            </div>
            </div>
        </div>
          </div>
  <?php include('./footer.php'); ?>
  <script>
  
    
    
    new Pidie();
  </script>


 <?php
    unset($_SESSION['error_in_adding']);
    unset($_SESSION['add_success']);
    unset($_SESSION['fname_error']);
    unset($_SESSION['lname_error']);
    unset($_SESSION['cname_error']);
    unset($_SESSION['website_error']);
    unset($_SESSION['cmail_error']);
    unset($_SESSION['address_error']);
    unset($_SESSION['CIN_error']);
    unset($_SESSION['PAN_error']);
    unset($_SESSION['GST_error']);
    unset($_SESSION['IEC_error']);
    unset($_SESSION['bank_error']);
    unset($_SESSION['INR_error']);
    unset($_SESSION['EURO_error']);
    unset($_SESSION['routing_error']);
    unset($_SESSION['swift_error']);
    unset($_SESSION['mobile_error']);
    unset($_SESSION['thank_error']);
 ?>

</body>

</html>
