

<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="sidenav-header  align-items-center">
        <a class="navbar-brand" href="./dashboard.php">
          <img src="./assets/images/logo.png" class="navbar-brand-img" alt="...">
        </a>
      </div>
      <div class="navbar-inner find">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="http://localhost:8000/invoice_task/dashboard.php">
                <i class="fa fa-desktop text-primary" style="color: white;"></i>
                <span class="nav-link-text">Dashboard</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="http://localhost:8000/invoice_task/index.php">
                <i class="fa fa-plus text-primary" style="color: white;"></i>
                <span class="nav-link-text">Create Invoice</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="http://localhost:8000/invoice_task/get_invoices.php">
                <i class="fa fa-file-text-o text-primary"></i>
                <span class="nav-link-text">Invoice List</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="http://localhost:8000/invoice_task/clients.php">
                <i class="fa fa-address-card-o text-primary"></i>
                <span class="nav-link-text">Clients</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="http://localhost:8000/invoice_task/projects.php">
                <i class="fa fa-tasks text-primary"></i>
                <span class="nav-link-text">Projects</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="http://localhost:8000/invoice_task/setting.php">
                <i class="fa fa-cog text-primary"></i>
                <span class="nav-link-text">Settings</span>
              </a>
            </li>
            
          </ul>
          <!-- Divider -->
          <!-- <hr class="my-3"> -->
          <!-- Heading -->
          
        </div>
      </div>
    </div>
  </nav>