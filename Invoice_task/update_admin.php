<?php

session_start();
require('./connection.php');

$email = $_GET['email'];

require('vendor/autoload.php');

use Rakit\Validation\Validator;
$validator = new Validator;

$validation = $validator->make($_POST + $_FILES, [
  'fname'                 => 'required',
  'lname'                 => 'required',
  'cname'                 => 'required',
  'website'               => 'required',
  'cmail'                 => 'required|email',
  'address'               => 'required',
  'CIN'                   => 'required',
  'PAN'                   => 'required',
  'GST'                   => 'required',
  'HSN'                   => 'required',
  'IEC'                   => 'required',
  'bank_name'             => 'required',
  'INR'                   => 'required|min:14|max:14',
  'EURO'                  => 'required|min:14|max:14',
  'routing'               => 'required',
  'swift_code'            => 'required',
  'mobile'                => 'required|min:10|max:10',
  'thank_you_message'     => 'required',
]);
$validation->validate();
if($_POST){
    if ($validation->fails()) {
        $errors = $validation->errors();
        $errors_array = $errors->firstOfAll();
        $_SESSION['fname_error'] = isset($errors_array['fname']) ? $errors_array['fname']: null;
        $_SESSION['lname_error'] = isset($errors_array['lname']) ? $errors_array['lname']: null;
        $_SESSION['cname_error'] = isset($errors_array['cname']) ? $errors_array['cname']: null;
        $_SESSION['website_error'] = isset($errors_array['website']) ? $errors_array['website']: null;
        $_SESSION['cmail_error'] = isset($errors_array['cmail']) ? $errors_array['cmail']: null;
        $_SESSION['address_error'] = isset($errors_array['address'])? $errors_array['address']: null;
        $_SESSION['CIN_error'] = isset($errors_array['CIN'])? $errors_array['CIN']: null;
        $_SESSION['PAN_error'] = isset($errors_array['PAN'])? $errors_array['PAN']: null;
        $_SESSION['GST_error'] = isset($errors_array['GST'])? $errors_array['GST']: null;
        $_SESSION['HSN_error'] = isset($errors_array['HSN'])? $errors_array['HSN']: null;
        $_SESSION['IEC_error'] = isset($errors_array['IEC'])? $errors_array['IEC']: null;
        $_SESSION['bank_error'] = isset($errors_array['bank_name'])? $errors_array['bank_name']: null;
        $_SESSION['INR_error'] = isset($errors_array['INR'])? $errors_array['INR']: null;
        $_SESSION['EURO_error'] = isset($errors_array['EURO'])? $errors_array['EURO']: null;
        $_SESSION['routing_error'] = isset($errors_array['routing'])? $errors_array['routing']: null;
        $_SESSION['swift_error'] = isset($errors_array['swift_code'])? $errors_array['swift_code']: null;
        $_SESSION['mobile_error'] = isset($errors_array['mobile'])? $errors_array['mobile']: null;
        $_SESSION['thank_error'] = isset($errors_array['thank_you_message'])? $errors_array['thank_you_message']: null;
        header('location:./setting.php');
        exit;
      }else{
          $sql = 'update company_details set first_name = "'.$_POST['fname'].'", last_name = "'.$_POST['lname'].'",company_full_title = "'.$_POST['cname'].'",website_address = "'.$_POST['website'].'",mail_address = "'.$_POST['cmail'].'", company_address = "'.$_POST['address'].'",CIN_number = "'.$_POST['CIN'].'", PAN_number = "'.$_POST['PAN'].'", GST_number = "'.$_POST['GST'].'",IEC_number = "'.$_POST['IEC'].'",HSN = '.$_POST['HSN'].', bank_name = "'.$_POST['bank_name'].'", 	INR_account_number = '.$_POST['INR'].', EURO_account_number = '.$_POST['EURO'].', routing_number = '.$_POST['routing'].', swift_code = "'.$_POST['swift_code'].'", mobile_number = '.$_POST['mobile'].', note1 = "'.$_POST['note1'].'", note2 = "'.$_POST['note2'].'", note3 = "'.$_POST['note3'].'", note4 = "'.$_POST['note4'].'", note5 = "'.$_POST['note5'].'", thank_you_message = "'.$_POST['thank_you_message'].'" where id = 1;';
          $run =  $conn->query($sql);
          if(!$run=== true){
            $error = mysqli_error($conn);
            $_SESSION['error_in_adding'] = $error;
            header('location:./setting.php');
          }else{
            $_SESSION['add_success'] = "User value updated successfully";
            header('location:./setting.php');
          }
      }
}
?>