<?php
session_start();
require('./connection.php');

$email = $_GET['email'];
$encode_id = urlencode(base64_encode($_GET['email']));
require('vendor/autoload.php');

use Rakit\Validation\Validator;
$validator = new Validator;

$validation = $validator->make($_POST + $_FILES, [
  'name'                  => 'required',
  'email'                 => 'required',
  'phone'                 => 'required',
  'gstin'                 => 'required',
  'currency'              => 'required',
  'address'               => 'required',
  'place_to_supply'       => 'required',
  'state_code'            => 'required',
]);
$validation->validate();
if($_POST){
  if ($validation->fails()) {
    $errors = $validation->errors();
    $errors_array = $errors->firstOfAll();
    $_SESSION['name_error'] = isset($errors_array['name']) ? $errors_array['name']: null;
    $_SESSION['email_error'] = isset($errors_array['email']) ? $errors_array['email']: null;
    $_SESSION['phone_error'] = isset($errors_array['phone']) ? $errors_array['phone']: null;
    $_SESSION['gstin_error'] = isset($errors_array['gstin']) ? $errors_array['gstin']: null;
    $_SESSION['currency_error'] = isset($errors_array['currency']) ? $errors_array['currency']: null;
    $_SESSION['address_error'] = isset($errors_array['address'])? $errors_array['address']: null;
    $_SESSION['place_error'] = isset($errors_array['place_to_supply'])? $errors_array['place_to_supply']: null;
    $_SESSION['state_error'] = isset($errors_array['state_code'])? $errors_array['state_code']: null;
    header('location:./edit_client.php?email='.$encode_id);
    exit;
  }else{
    $sql = 'update clients set name = "'.$_POST['name'].'",email = "'.$_POST['email'].'",phone = '.$_POST['phone'].',gstin = "'.$_POST['gstin'].'",currency = "'.$_POST['currency'].'",address = "'.$_POST['address'].'",place_of_supply = "'.$_POST['place_to_supply'].'",state_code = "'.$_POST['state_code'].'" where email = "'.$email.'";';
    $run =  $conn->query($sql);
    if(!$run=== true){
        $error = mysqli_error($conn);
        $_SESSION['error_msg'] = $error;
        
        header('location:./edit_client.php?email="'.$encode_id.'"');
    }

    $id = 'select id from clients where email = "'.$_GET['email'].'";';
    $run_id =  $conn->query($id);
    $result_id = mysqli_fetch_array($run_id);	

    $temp = 'select client_id,tax_name from client_taxes WHERE client_id='.$result_id['id'].';';
    $temp_result = $conn->query($temp);
    while($row2 = $temp_result->fetch_array(MYSQLI_ASSOC)){
        $data[] = $row2['tax_name'];
    }

    $compare = array_diff($data,$_POST['tax']);
    if(count($compare) > 0){
        $a = array_values($compare);
        print_r($a);
        for($j = 0;$j < count($compare);$j++){
            $delete_extra_tax = 'delete from client_taxes where client_id='.$result_id['id'].' and tax_name = "'.$a[$j].'";';
            echo $delete_extra_tax;
            $del = $conn->query($delete_extra_tax);
            if(!$del){
                $error = "Error inserting data";
                $_SESSION['error_msg'] = $error;
                // header('location:./edit_client.php?email='.$_POST['email']);
            }
        }
    }
    for($i = 1;$i<count($_POST['tax']);$i++){

            $sqli = 'SELECT count(percentage) FROM client_taxes WHERE client_id='.$result_id['id'].' and tax_name = "'.$_POST["tax"][$i].'" GROUP BY client_id,tax_name HAVING count(percentage) > 0';
            $run = $conn->query($sqli);
            
        if ($run->num_rows == 0) {
                $insertNewTax = 'INSERT INTO client_taxes (client_id,tax_name,percentage) VALUES ("'.$result_id['id'].'","'.$_POST['tax'][$i].'",'.$_POST['tax_rate'][$i].');';
            if (!$conn->query($insertNewTax) == TRUE) {
                    $error = "Error inserting data";
                    $_SESSION['error_msg'] = $error;
                    header('location:./edit_client.php?email='.$encode_id);
            } 
            else{
                $_SESSION['add_success'] = "User value updated successfully";
                header("location:./clients.php");
            }
        }else{
            $sql2 = 'update client_taxes set tax_name = "'.$_POST['tax'][$i].'", percentage = '.$_POST['tax_rate'][$i].' where client_id = '.$result_id['id'].' AND tax_name = "'.$_POST['tax'][$i].'";';
            // echo $sql2;
            $run2 = $conn->query($sql2);
            // print_r($run2);
            if(!$run2){
                    $error = mysqli_error($conn);
                    $_SESSION['error_msg'] = $error;
                    header('location:./edit_client.php?email='.$encode_id);
                }else{
                    $_SESSION['add_success'] = "User value updated successfully";
                    header("location:./clients.php");
                }
            }
    }
}
}
?>