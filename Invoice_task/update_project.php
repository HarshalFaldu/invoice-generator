<?php
session_start();
require('./connection.php');

$c_id = $_GET['id'];
// $project_name = $_GET['project_name'];
$encode_id = urlencode(base64_encode($c_id));
require('vendor/autoload.php');

use Rakit\Validation\Validator;
$validator = new Validator;

$validation = $validator->make($_POST + $_FILES, [
  'project_name'          => 'required|min:1',
  'description'           => 'required|min:1',
  'is_hourly_based'       => 'required',
]);
$validation->validate();
if($_POST){
    if ($validation->fails()) {
        $errors = $validation->errors();
        $errors_array = $errors->firstOfAll();
        $_SESSION['Pname_error'] = isset($errors_array['project_name']) ? $errors_array['project_name']: null;
        $_SESSION['desc_error'] = isset($errors_array['description'])? $errors_array['description']: null;
        $_SESSION['hourly_error'] = isset($errors_array['is_hourly_based'])? $errors_array['is_hourly_based']: null;
        // $_SESSION['rate_error'] = isset($errors_array['rate_per_hour'])? $errors_array['rate_per_hour']: null;
        
        header('location:./edit_project.php?id='.$encode_id);
        exit;
    }else{
        if(!isset($_POST['rate_per_hour'])){
            $number = '-';  
          }else{
            $number = str_replace(['+', '-'], '', filter_var($_POST['rate_per_hour'], FILTER_SANITIZE_NUMBER_INT));
            number_format($number);
          }
        $sql = 'update projects set project_name = "'.$_POST['project_name'].'",project_description = "'.$_POST['description'].'",is_hourly_based = "'.$_POST['is_hourly_based'].'",rate_per_hour = "'.$_POST['rate_per_hour'].'" where id = '.$c_id.';';
        $run =  $conn->query($sql);
        if(!$run=== true){
            $error = mysqli_error($conn);
            $_SESSION['error_msg'] = $sql;
            header('location:./edit_project.php?id='.$encode_id);
        }else{
            $_SESSION['add_success'] = "User value updated successfully";
            header("location:./projects.php");
        }
    }
}
?>