# Invoice Generator #

This repository contains all the files for project Invoice generation with sql file and all required plugin files.

### Step 1 ###

* Clone or download the repository into your localhost( If using xampp then clone it in the folder named "htdocs")

### Step 2 ###

* Import the sql file in the phpmyadmin database in the localhost.

### Step 3 ###

* Setup the connection.php file as per your database name

### Step 4 ###

* Start your server and open the link ("your localhost id")+invoice_task/login.php

### Main pages details ###

* login.php -> Used as a login or signin page
* dashboard.php -> Show overall data in the system
* index.php -> Page from where user can generate a invoice
* clients.php -> Show all active clients and we can add, update or delete clients from that page
* removed_clients.php -> Show all removed clients and can able to restore clients from that page
* projects.php -> Show all active projects and we can add, update or delete projects from that page
* removed_projects.php -> Show all removed projects and can able to restore projects from that page
* edit_client.php, edit_project.php, edit_user.php -> We can edit client/project/user from that page
* get_invoices.php -> Show all active invoices and can able to delete invoice from that page
* removed_invoice -> Show all removed invoices and can able to restore invoices from that page
* settings.php -> Page from where admin can edit the company details
* logout.php -> Used to logout user
* invoice_system.sql -> Sql file which you can import in your database